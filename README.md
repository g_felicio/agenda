# Phonebook/Agenda
Phonebook  
Using Laravel Framework, version 5.2.45 and HTML.

Using jquery-2.2.1 in a minified file.

Using Bootstrap v3.3.6.

Project developed for education purposes, so I could learn the framework.

REQUIRES LARAVEL COLLECTIVE

https://laravelcollective.com/docs/5.2/html

-- click the link for an installation walkthrough and documentation


-----------------------------------------------------------------------------------------------------------------------------

STEP BY STEP 
- Create a new Laravel project. This project was developed using Laravel 5.2.45, so if you use an older or newer version of the framework, make sure to tweak the code to fit your version.
- First thing: run "php artisan make:auth" command (or a similar command) to create the authorization scaffold
* Step made early in the project to avoid any error/conflict/problem down the road
- Set up database, migrations (use migration files as REFERENCE) and then migrate your tables
- Use .blade files as reference to insert new contacts in your table 
- Do as you like

-----------------------------------------------------------------------------------------------------------------------------

HIGHLY RECOMMENDED

-- Start your own Laravel 5.2 project. The code might not work properly if your framework is in a different version.


-----------------------------------------------------------------------------------------------------------------------------

HOW THE APP WORKS -- SIMPLE OVERVIEW ++ IMPORTANT STUFF

You'll have to register in the app LOCALLY and your info will be saved in the users table.

(So, remember to migrate your tables, that's important.)

This info will be required so you can load the entirety of the phonebook view, which contains the form to record a contact in the table contacts

(You can check who is registered in the /users url, which will load the users.blade.php view. Feel free to check and tweak it.)

Also, you can only record new contacts in this table if you are logged in

You can check all the contacts recorded in the 'contacts table' in the /allcontacts url. This url was defined just to keep track of all the contacts recorded in the table, but it won't give you write privileges over them, unless you are logged in as the user who created a contact. 
UPDATE AUGUST: view removed from project.

The table is sorted using a ContactController logic to show just the contacts you recorded in it, which means you can't see the contacts other people recorded in this table.

In your contacts table, you can edit the contact, upload a photo to its record or delete it, but only if YOU were the one to record it in the table, make sure to remember this.
UPDATE JULY: added dataTables to contacts table for easier management.

UPDATE AUGUST:
In the contacts.blade.php view, added a new section of code that orders the contacts in a stylized widget that holds the required information. It's pretty for the eyes, but lacks an easy management provided with the dataTables implementation. If you prefer this type of presentation, just comment out the html tags for the table and uncomment the widget implementation. Or vice-versa, your choice. The widget code is separated from the table code using a comment that marks the end of the table code and the beginning of the widget code. Also, you can dismember these bits of code, put each in its own view and choose from there.

Do as you like.

Also 2, the app has some css files in the public folder. These files are linked in the views. Fell free to mess with them, move them, delete them... do anything.

The name of the css files are:
theNameOfTheView + st + .css

Feel free to edit these codes as you see fit.

There is an UsersTableSeeder file that seeds the contacts table with some entries, so it becomes easier to test. Even though the file is named UsersTableSeeder by Laravel, so it is a default name, it seeds the contacts table. Feel free to create a new seeder or edit this one in any way you like.

-----------------------------------------------------------------------------------------------------------------------------


THE LOOKS OF THE PROJECT

The pages created with the php artisan make:auth command line uses the default bootstrap provided with the framework.

The pages I created for the app (phonebook, contacts, edit, delete, upload and some others) uses simple CSS styling, just so it wouldn't be a blank style page.

JULY - Bootstrap added in view files.


-----------------------------------------------------------------------------------------------------------------------------

FEEL FREE TO IMPROVE THE APP IN ANY WAY YOU LIKE

-------------------------------------------------------------------------------------------------------------------------------

Status so far (27th Apr 2017) - TOTAL FAILURE

Status so far (28th Apr 2017) - LOOKING UP

Status so far (2nd May 2017)  - CHANGING MACHINE - BACK TO SQUARE ZERO - =(

Status so far (22nd May 2017) - PROJECT COMPLETE

Status from May to August     - SOME USEFUL IMPLEMENTATIONS TO PROJECT

-----------------------------------------------------------------------------------------------------------------------------

Gustavo Felício