var gulp = require('gulp');
var	less = require('gulp-less');
var	browserify = require('browserify');
var	elixir = require('laravel-elixir');
var copy = require('gulp-copy');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var jquery = require('jquery');

var jsPath = 'resources/assets/js';
var lsPath = 'resources/assets/less';
var btPath = 'docs/nifty-v2.4';
var pbDest = 'public/';
var jsDest = 'public/js/';
var csDest = 'public/css';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// elixir(function(mix) {
//     mix.sass('app.scss');
// });

gulp.task('copyJsFiles', function(){
	gulp.src([
		'docs/nifty-v2.4/demo/plugins/fast-click/fastclick.min.js',
		'docs/nifty-v2.4/demo/plugins/switchery/switchery.min.js',
		'docs/nifty-v2.4/demo/plugins/bootstrap-select/bootstrap-select.min.js',
		'docs/nifty-v2.4/demo/plugins/morris-js/morris.min.js',
		'docs/nifty-v2.4/demo/plugins/morris-js/raphael-js/raphael.min.js',
		'docs/nifty-v2.4/demo/plugins/sparkline/jquery.sparkline.min.js',
		'docs/nifty-v2.4/demo/plugins/flot-charts/jquery.flot.min.js',
		'docs/nifty-v2.4/demo/plugins/flot-charts/jquery.flot.resize.min.js',
		'docs/nifty-v2.4/demo/plugins/flot-charts/jquery.flot.pie.min.js',
		'docs/nifty-v2.4/demo/plugins/gauge-js/gauge.min.js',
		'docs/nifty-v2.4/demo/plugins/easy-pie-chart/jquery.easypiechart.min.js',
		'docs/nifty-v2.4/demo/js/demo/nifty-demo.min.js',
		'docs/nifty-v2.4/demo/js/demo/charts.js',
		'docs/nifty-v2.4/demo/js/bootstrap.min.js',
		'docs/nifty-v2.4/demo/js/jquery-2.2.1.min.js',
		'docs/nifty-v2.4/demo/js/nifty.min.js',
		'docs/nifty-v2.4/demo/plugins/bootbox/bootbox.min.js',
		'docs/nifty-v2.4/demo/js/demo/ui-modals.js',
		'docs/nifty-v2.4/demo/plugins/datatables/media/js/jquery.dataTables.js',
		'plugins/datatables/media/js/dataTables.bootstrap.js',
		'plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js',
		'node_modules/datatables-bootstrap3-plugin/media/js/datatables-bootstrap3.js',
		'node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
		'node_modules/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js',
		'node_modules/bootstrap-datepicker/js/bootstrap-datepicker.js',
		'node_modules/bootstrap/dist/js/bootstrap.js',
	])
	.pipe(gulp.dest(jsDest))
	.pipe(notify('Cópias de Arquivos js Finalizados!'))
});

gulp.task('CopyCsFiles', function(){
	gulp.src([
		'docs/nifty-v2.4/demo/plugins/switchery/switchery.min.css',
		'docs/nifty-v2.4/demo/plugins/bootstrap-select/bootstrap-select.min.css',
		'docs/nifty-v2.4/demo/plugins/morris-js/morris.min.css',
		'docs/nifty-v2.4/demo/css/bootstrap.min.css',
		'docs/nifty-v2.4/demo/css/nifty.min.css',
		'docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css',
		'docs/nifty-v2.4/demo/plugins/datatables/media/css/dataTables.bootstrap.css',
		'docs/nifty-v2.4/demo/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css',
	])
	.pipe(gulp.dest(csDest))
	.pipe(notify('Cópias de Arquivos CSS Finalizados!'))
});

gulp.task('serve', [
	'copyJsFiles',
	'CopyCsFiles'
]);