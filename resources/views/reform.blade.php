<!DOCTYPE html>
<html>
    <head>
        @php
            $user = Auth::user()->id;
        @endphp
        <meta charset="utf-8">

        <title>
            Phonebook - Edit Information  
        </title>

        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
    </head>
    <body> 
        @include('partials.header')<br/><br/>
        <div class="container">
            @include('partials.userReform')
        </div>
    </body>
</html>