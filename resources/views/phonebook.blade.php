<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        
        <script src="/js/jquery-2.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/nifty.min.js"></script>
        <script src="js/bootbox.min.js"></script>
        <script src="js/ui-modals.js"></script>
        <script src="js/modalSaver.js"></script>

        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('css/infost.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
        
        <title>
            Phonebook - Add a new contact 
            @if(\Auth::check())
                {{',  ' . \Auth::user()->name }}
            @endif
        </title>
    </head>

    <body>
        @include('partials.header')
        
        <div class="container">
            @if(Auth::check())
                <br/><br/>
                @include('partials.contactForm')
            @else
                @include('partials.info')
            @endif
        </div>

        <script type="text/javascript">
            $('#modalForm').submit(function(e){
                e.preventDefault();
                var $form = $(this);

                // get url from action attribute
                 var $url = $(this).attr('action');

                $.ajax({
                    method: 'POST',
                    url: $url,
                    data: $form.serialize()
                })
                .done(function(res){

                    // make new option based on ajax response
                    var option = '<option class="category"  selected="true" value="'+res.id+'">'+res.category+'</option>';

                    // append option to category select
                    $('#category').append(option);
                });
                $('#demo-default-modal').modal('hide');
            })
        </script>
    </body>
</html>