<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Edit Information
                </h3>
            </div>

            {!! Form::open(['route' => ['user.update'], 'method' => 'PUT', 'enctype' => 'multipart/form-data'], Auth::user()->id) !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class = "form-group">
                                {!! Form::label('user_image', 'Upload an image!') !!}
                                {!! Form::file('user_image')!!}
                            </div><br/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class = "form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', Auth::user()->name) !!}
                            </div><br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="form-group">
                                {!! Form::label('email', 'E-Mail') !!}
                                {!! Form::text('email', Auth::user()->email) !!}
                            </div><br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 text-right">
                            <a><button class="btn btn-primary fa fa-send-o" type="submit"> Update Information </button></a>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>