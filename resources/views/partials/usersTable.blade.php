<h1>
    Users
</h1>

            
<table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0">
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th> 
                Name
            </th>
            <th>
                Image
            </th>
            <th>
                E-Mail
            </th>
            <th>
                Password
            </th>
            <th>
                Created At
            </th>
            <th>
                Updated At
            </th>
            <th>
                Remember Token
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($user as $user)
            <tr>
                <td>
                    {{ $user->id }}
                </td>
                <td>
                    {{ $user->name }}
                </td>
                <td> 
                    <img src="{!! asset('public/user' . $user->user_image) !!}" height="120" width="92" class="imCell">
                </td>
                <td>
                    {{ $user->email }}
                </td>
                <td>
                    {{ $user->password }}
                </td>
                <td>
                    {{ $user->created_at }}
                </td>
                <td>
                    {{ $user->updated_at }}
                </td>
                <td>
                    {{ $user->remember_token }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>