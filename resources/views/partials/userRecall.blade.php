<div class="content">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-warning panel-bordered">
				<div class="panel-heading">
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4 col-md-offset-1">
							<span>
								{{ Auth::user()->name }} 
							</span>
						</div>
						<div class="col-md-4 col-md-offset-1">
							<span>
								{{ Auth::user()->email }} 
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-danger panel-bordered">
				<div class="panel-heading">
					<h2 class="panel-title del_info">
						<i class="fa fa-exclamation-triangle"></i>
							<strong>
								Remember, all your contacts will be deleted too and this action can't be reverted 
							</strong>
						<i class="fa fa-exclamation-triangle"></i>
					</h2>
				</div>
				<div class="panel-body">
					<br/>
					<h3 class="del_infoPro">
						<i class="fa fa-angle-double-right"></i>
							Proceed anyway?
						<i class="fa fa-angle-double-left"></i>
					</h3>
					<br/>
				</div>
			</div>
			<div class="dec_links">
				<a href="/dels/{{ Auth::user()->id }}" class="confirmation fa fa-check-circle-o btn btn-danger">
					YES, delete my account
				</a>
				<a href="/home" class="denial fa fa-times-circle-o btn btn-primary"> 
					NO, take me home 
				</a>
			</div>
		</div>
	</div>
</div>