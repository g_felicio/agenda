<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title">
                    Contact Form
                </h2>
            </div>
            {!! Form::open(['route' => ['contacts.create'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group
                                @if($errors->has('category'))
                                    has-error
                                @endif">
                                {!! Form::label('category', 'Choose Your Contact Category') !!}
                                <select name="category" class="form-control" id="category">
                                    <option disabled="true" selected="true">
                                        List
                                    </option>
                                    @foreach ($categories as $category)
                                        <option name="category" id="category" class="category" value="{!! $category->id !!}">
                                            {!! $category->category !!}
                                        </option>
                                    @endforeach
                                    @if($errors->has('category'))
                                        <p class="help-block">
                                            <span class="label label-danger">Warning</span> {{ $errors->first('category') }}
                                        </p>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>
                                Create Your Contact Category
                            </label><br/>
                            <button type="button" data-toggle="modal" data-target="#demo-default-modal" class="btn btn-default">
                                <i class="fa fa-plus"></i> Create New Category
                            </button>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class = "form-group
                                @if($errors->has('name'))
                                    has-error
                                @endif"> 
                                {!! Form::label('name', 'Name *') !!}
                                {!! Form::text('name', old('name'),['class'=>'form-control', 'placeholder' => 'Name of your contact']) !!}
                                @if($errors->has('name')) 
                                    <p class = "help-block"><span class="label label-danger">Warning</span> {{ $errors->first('name') }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class = "form-group 
                                @if($errors->has('lastname'))
                                    has-error
                                @endif"> 
                                {!! Form::label('lastname', 'Lastname') !!}
                                {!! Form::text('lastname', old('lastname'),['class'=>'form-control', 'placeholder' => 'Lastname of your contact']) !!}
                                @if($errors->has('lastname'))
                                    <p class = "help-block"><span class="label label-danger">Warning</span> {{ $errors->first('lastname') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class = "form-group 
                                @if($errors->has('email'))
                                    has-error
                                @endif">
                                {!! Form::label('email', 'E-Mail *') !!}
                                {!! Form::text('email', null,['class'=>'form-control', 'placeholder' => 'If your contact does not have one use contactName@email.com']) !!}
                                @if($errors->has('email'))
                                    <p class = "help-block"><span class="label label-danger">Warning</span> {{ $errors->first('email') }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class = "form-group
                                @if($errors->has('phone'))
                                    has-error
                                @endif">
                                {!! Form::label('phone', 'Phone Number *') !!}
                                {!! Form::text('phone', null,['class'=>'form-control', 'placeholder' => 'If your contact does not have one, just insert three numbers at random']) !!}
                                @if($errors->has('phone'))
                                    <p class="help-block"><span class="label label-danger">Warning</span> {{ $errors->first('phone') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class = "form-group">
                                {!! Form::label('address', 'Address') !!}
                                {!! Form::text('address', old('address'),['class'=>'form-control', 'placeholder' => 'Street name, number, neighborhood, city, state initials, country']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class = "form-group">
                                {!! Form::label('description', 'Description') !!}
                                {!! Form::text('description', old('description'),['class'=>'form-control', 'placeholder' => 'Something to describe your contact. Use a simple word, like family, work, friend and such']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class = "form-group">
                                {!! Form::label('image', 'Upload an image!') !!}
                                {!! Form::file('image')!!}
                            </div>
                        </div>
                    </div>                
                    <div class="row">
                        <div class="col-md-6">
                            <p class="text-left">
                                <strong> * = Required field (Can't be blank) </strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class = "text-right buttons">
                                {!! Form::button(' Submit Information', ['type' => 'submit', 'class' => 'btn btn-primary fa fa-paper-plane-o']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- MODAL --}}

<div class="modal fade demo-default-modal" name="demo-default-modal" id="demo-default-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">
                    Create Category
                </h3>
            </div>
            
            {!! Form::open(['route'=>['category.create'], 'method'=>'POST', 'class'=>'modalForm', 'id'=>'modalForm', 'name'=>'modalForm']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group
                                @if($errors->has('category'))
                                    has-error
                                @endif">
                                {!! Form::label('category', 'Category') !!}
                                {!! Form::text('category', NULL, ['placeholder'=>'Category Name', 'class'=>'form-control']) !!}
                                @if($errors->has('category'))
                                    <p class="help-block">
                                        <span class="label label-danger">Warning</span> {{ $errors->first('category') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('Close', ['type' => 'button', 'class' => 'btn btn-default', 'data-dismiss'=>'modal']) !!}
                    {!! Form::submit('Save Category', ['type' => 'submit', 'class' => 'btn btn-primary modalSave', 'name' => 'modalSave', 'id' => 'modalSave', 'hide'=>'true']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>