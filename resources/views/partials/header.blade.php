<div class = "header">
	<ul>
		@if(Auth::guest())
        	<li> <a href="/home" class = "hd_link"><i class="fa fa-home"></i> Home </a> </li>
        @endif

        <li> <a href="/phonebook" class="phb_link"><i class="fa fa-plus-circle"></i> Add New Contact </a> </li>
        <li> <a href="/contacts" class = "con_link"><i class="fa fa-table"></i> View Contacts </a> </li>
        
         @if(Auth::guest())
            <li> <a href="/login" class = "log_link"><i class="fa fa-sign-in"></i> Login </a> </li>	                       
            <li> <a href="/register" class = "reg_link"><i class="fa fa-users"></i> Register </a> </li>
        @else
            <span class="dropdown">
                <div class="usr_link">                                          
                    <li> <a href="/home" class="fa fa-user"> {{ Auth::user()->name }} </a> </li>
                </div>
                <span class="dropdown-content">
                    <li> <a href="{{ url('/logout') }}" class="fa fa-sign-out"> Logout </a></li>
                </span>
            </span>
        @endif            
	</ul>
</div>