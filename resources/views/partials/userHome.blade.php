<div class="panel-widget bg-gray">
    <div class="widget-header bg-dark"></div>
    <div class="widget-body text-center">
        <img alt="Profile Picture" class="widget-img img-md img-border" src="{!! asset('public/user/' . $user->user_image) !!}">
        <h4 class="mar-no">
            {{ Auth::user()->name}} 
        </h4>
        <p class="text-muted mar-btm"> 
            {{ Auth::user()->email }} 
        </p>
        <div class="pad-ver btnCenter">
            <a href="/reform/{{Auth::user()->id}}">
                <button class="btn btn-primary fa fa-edit">
                    Edit Your Info
                </button>
            </a>
            <a href="/recall/{{Auth::user()->id}}">
                <button class="btn btn-warning fa fa-times-circle-o">
                    Delete Account
                </button>
            </a>
            <br/>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary panel-colorful">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <i class="fa fa-list-ol sizeUp mar-lft"></i>
                    </div>
                    <div class="col-md-7 text-center">
                        <span class="text-4x text-bold">
                            {{ $contactCount }}
                        </span>
                        <span class="text-2x">
                            contacts
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary panel-colorful">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <i class="fa fa-list sizeUp mar-lft"></i>
                    </div>
                    <div class="col-md-7 text-center">
                        <span class="text-4x text-bold">
                            {{ $categoryCount }}
                        </span>
                        <span class="text-2x">
                            categories
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-warning panel-colorful">
            <h3 class="panel-title text-center">
                <i class="fa fa-pie-chart sizeUp mar-top"></i>
                Your Contacts By Category
            </h3>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="categoriesDonut" style="height:400px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>