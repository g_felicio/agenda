<br/>
<div class="panel panel-bordered panel-warning">
	<div class="panel-heading">
		<h1 class="panel-title h_title">
			Account Deleted 
		</h1>
	</div>
	<div class="panel-body">
		<div class="us_del">
			Parting is such sweet sorrow. <br/><br/>
			We hope to see you again.<br/>
			Until then I bid you farewell.
		</div>
	</div>
</div>