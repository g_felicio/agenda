<div class="col-md-6 col-md-offset-3">
    <div class="panel panel-primary panel-bordered">
        <div class="panel-heading">
            <h3 class="panel-title text-center">
                LOGIN
            </h3>
        </div>

        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">
                                E-Mail Address
                            </label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter e-mail">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">
                                Password
                            </label>
                            <input id="password" type="password" class="form-control" name="password" placeholder="Enter password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group">                                
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary fa fa-sign-in">
                            Login
                        </button>
                    </div>
                </div>
            </div>
        </form> 

        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ url('/password/reset') }}">
                    <button class="btn btn-warning fa fa-question">
                        Forgot Password
                    </button>
                </a>
            </div>
        </div>
        <br/>      
    </div>
</div>