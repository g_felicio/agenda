<br/>
<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-colorful panel-danger">
		<div class="panel-heading text-center">
			<h1 class="panel-title">
				<i class="fa fa-warning"></i>
				ATTENTION
				<i class="fa fa-warning"></i>
			</h1>
		</div>
		<div class="row">
			<div class="col-md-12">				
				<h2> You need to login or sign up to view this page </h2><br/>				
			</div>
		</div>
	</div>
</div>