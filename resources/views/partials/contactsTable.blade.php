<div class="row">
	<div class="col-md-12">
		<div class = "top_ret">
			<a href="/phonebook" class="btn btn-default fa fa-plus-circle">
				Add New Entry
			</a>
		</div>
	</div>
</div>

<br/><br/>

<div class="row">
	<div class="col-md-12">
		<div class="panel bg-trans">
			<div class="panel-heading">
				<h3 class="panel-title">
					Your Contacts
				</h3>
			</div>

			<div class="panel-body">
				<table id="demo-dt-basic" class="table table-striped table-bordered dataTable no-footer text-center" cellspacing="0" width="100%" role="grid" aria-describedby="demo-dt-basic info">

					<thead class="bg-gray-dark">
						<tr role="row">
							<th class="text-center sorting">
								ID
							</th>
							<th class="text-center sorting">
								User
							</th>
							<th class="text-center sorting">
								Image
							</th>
							<th class="text-center sorting">
								Name
							</th>
							<th class="text-center sorting">
								Lastname
							</th>
							<th class="text-center sorting">
								E-Mail
							</th>
							<th class="text-center sorting">
								Phone
							</th>
							<th class="text-center sorting">
								Address
							</th>
							<th class="text-center sorting">
								Description
							</th>
							<th class="text-center sorting">
								Category
							</th>
							<th class="text-center sorting">
								Actions
							</th>
						</tr>
					</thead>

					<tbody>
						@foreach($contacts as $contact)
							<tr class="tableBody" role="row">
								<td class="sorting">
									{{ $contact->id }}
								</td>
								<td class="sorting">
									{{ $contact->user_id }}
								</td>
								<td class="sorting">
									<img class="img-circle img-md" src="{!! asset('public/uploads/' . $contact->image) !!}">
								</td>
								<td class="sorting">
									{{ $contact->name }}
								</td>
								<td class="sorting">
									{{ $contact->lastname }}
								</td>
								<td class="sorting">
									{{ $contact->email }}
								</td>
								<td class="sorting">
									{{ $contact->phone }}
								</td>
								<td class="sorting">
									{{ $contact->address }}
								</td>
								<td class="sorting">
									{{ $contact->description }}
								</td>
								<td class="sorting">
									{{ $contact->category_id }}
								</td>
								<td class="sorting">
									<span class="dropdown">
										<li>
											<i class="fa fa-list-ul"></i>
											Actions
										</li>
										<span class="dropdown-content_tbl">
											<a href="/edit/{{ $contact->id }}">
												<i class="fa fa-edit"></i>
												Edit
											</a>
											<br/><br/>
											<a href = "deleteconfirm/{{ $contact->id }}">
												<i class="fa fa-times"></i>
												Delete
											</a>
											<br/><br/>
											<a href="/upload/{{ $contact->id }}">
												<i class="fa fa-file-picture-o"></i>
												Update Photo
											</a>
										</span>
									</span>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-md-12">
		<div class = "ret">
			<a href = "/phonebook" class="btn btn-default fa fa-plus-circle">
				Add New Entry
			</a>
		</div>
	</div>
</div>

{{-- TESTS ON CONTACTS WIDGETS --}}
{{-- FEEL FREE TO COMMENT OUT THE METHOD YOU WANT TO SHOW YOUR CONTACTS, TABLE OR WIDGET --}}

{{-- <div class="row">
	<div class="col-md-12">
		<div class = "top_ret">
			<a href = "/phonebook" class="btn btn-default fa fa-plus-circle">
				Add New Entry
			</a>
		</div>
	</div>
</div><br/>

@foreach($contacts as $contact)
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel-widget bg-gray">
				<div class="widget-header bg-dark"></div>
				<div class="widget-body text-center">
				    <img alt="Profile Picture" class="widget-img img-circle img-md img-border" src="{!! asset('public/user/' . $contact->image) !!}">
				    <h4 class="mar-no">
				        {{ $contact->name }} {{ $contact->lastname }} 
				    </h4>
				    <p class="text-muted mar-btm"> 
				        {{ $contact->email }} 
				    </p>
				    <p class="text-muted mar-btm"> 
				        {{ $contact->phone }} 
				    </p>
				    <div class="pad-ver btnCenter">
				        <a href="/edit/{{ $contact->id }}">
				            <button class="btn btn-primary fa fa-edit">
				                Edit Contact Info
				            </button>
				        </a>
				        <a href="upload/{{ $contact->id }}">
				            <button class="btn btn-warning fa fa-times-circle-o">
				                Update Image
				            </button>
				        </a>
				        <a href="deleteconfirm/{{ $contact->id }}">
				            <button class="btn btn-warning fa fa-times-circle-o">
				                    Delete Account
				            </button>
				        </a>
				        <br/>
				    </div>
				</div>
			</div>
		</div>
	</div>
@endforeach

<div class="row">
	<div class="col-md-12">
		<div class = "ret">
			<a href = "/phonebook" class="btn btn-default fa fa-plus-circle">
				Add New Entry
			</a>
		</div>
	</div>
</div> --}}