<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-primary panel-bordered">
			<div class="panel-heading">
				<h1 class="panel-title text-center">
					Upload Image
				</h1>
			</div>
			
			{!! Form::model($contact, [ 'method' => 'PUT', 'route' => ['contacts.upload', $contact->id], 'enctype' => 'multipart/form-data']) !!}
				<div class="panel-body">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							{!! Form::file('image',['class' => 'btn btn-default']) !!}
						</div>
					</div><br/>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">	    
							{!! Form::submit('Submit Image',['class' => 'btn btn-primary']) !!}
						</div>
					</div>			
				</div><br/>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 text-right">
		<a href = "/phonebook" class="btn btn-warning fa fa-plus-circle">
			Add New Entry 
		</a>
	</div>
	<div class="col-md-6 text-left">
		<a href="/contacts" class="btn btn-warning fa fa-times-circle">
			View Contacts   
		</a>
	</div>
</div>