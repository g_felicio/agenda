<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <div class="panel panel-primary panel-bordered">
            <div class="panel-heading">
                <h2 class="panel-title text-center">
                    Edit Contact
                </h2>
            </div>
            {!! Form::open(['method' => 'PUT', 'route' => [ 'contacts.update', $contact->id]]) !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class = "form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', $contact->name, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class = "form-group"> 
                                {!! Form::label('lastname', 'Lastname') !!}
                                {!! Form::text('lastname', $contact->lastname, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class = "form-group 
                                @if($errors->has('email')) 
                                    has-error
                                @endif">
                                {!! Form::label('email', 'E-Mail') !!}
                                {!! Form::text('email', $contact->email, ['class'=>'form-control']) !!}
                                @if($errors->has('email')) 
                                    <p class = "help-block">{{ $errors->first('email') }}</p> 
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class = "form-group 
                                @if($errors->has('phone'))
                                    has-error
                                @endif">
                                {!! Form::label('phone', 'Phone Number') !!}
                                {!! Form::text('phone', $contact->phone, ['class'=>'form-control']) !!}
                                @if($errors->has('phone')) 
                                    <p class="help-block">{{ $errors->first('phone') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class = "form-group">
                                {!! Form::label('address', 'Address') !!}
                                {!! Form::text('address', $contact->address, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class = "form-group">
                                {!! Form::label('description', 'Description') !!}
                                {!! Form::text('description', $contact->description, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="/contacts">
                                <button type="submit" class="btn btn-primary fa fa-check-circle">
                                    Update Information 
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <a href="/contacts">
                    <button type="button" class="btn btn-warning fa fa-times-circle">
                        View Contacts
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>