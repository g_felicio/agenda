<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-warning panel-colorful">
				<div class="panel-heading">
					<h1 class="panel-title">
						<div class="del_title">
							<i class="fa fa-warning"></i>
								DELETE CONTACT?
							<i class="fa fa-warning"></i>
						</div>
					</h1>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<h4 class="text-center"> 
								{!! $contact->name !!} 
							</h4>
							<h4 class="text-center"> 
								{!! $contact->lastname !!} 
							</h4>
							<h4 class="text-center"> 
								{!! $contact->email !!} 
							</h4>
							<h4 class="text-center"> 
								{!! $contact->phone !!} 
							</h4>
							<h4 class="text-center"> 
								{!! $contact->address !!} 
							</h4>
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<div class="row">
						<div class="col-md-12">							
							<a href="/delete/{{ $contact->id }}">
								<button class="btn btn-danger">
									<i class="fa fa-check"></i>
									Yes, delete contact
								</button>
							</a>
							
							<a href="/contacts">
								<button class="btn btn-primary">
									<i class="fa fa-times"></i>
									No, take me back
								</button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>