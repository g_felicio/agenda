<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h1 class="text-danger"> 
				Contact Deleted! 
			</h1>
		</div>
	</div>
	<br/><br/>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<a href="/phonebook">
				<button class="btn btn-primary">
					<i class="fa fa-plus-circle"></i>
					Create New Contact
				</button>
			</a>							
			<a href="/contacts">
				<button class="btn btn-primary">
					<i class="fa fa-table"></i>
					View Contacts
				</button>
			</a>
		</div>
	</div>
</div>