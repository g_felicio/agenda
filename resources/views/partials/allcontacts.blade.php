<div class="row">
	<div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">
				All Contacts
			</h3>
		</div>
		<div class="panel-body">
			<table id="demo-dt-basic" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th class="min-tablet">
							ID
						</th>
						<th>
							Image		
						</th>
						<th>
							Name		
						</th>
						<th>
							Lastname	
						</th>
						<th>
							E-Mail		
						</th>
						<th>
							Phone		
						</th>
						<th>
							Address		
						</th>
						<th>
							Description 
						</th>
						<th>
							Created By	
						</th>
						<th>
							Actions		
						</th>
					</tr>
				</thead>

				<tbody>
					@foreach($contacts as $contact)
						<tr>
							<td>
								{{ $contact->id }}
							</td>
							<td>
								<img src="{!! asset('public/uploads/' . $contact->image) !!}" height="120" width="92" class="imCell">
							</td>
							<td>
								{{ $contact->name }}
							</td>
							<td>
								{{ $contact->lastname }}
							</td>
							<td>
								{{ $contact->email }}
							</td>
							<td>
								{{ $contact->phone }}
							</td>
							<td>
								{{ $contact->address }}
							</td>
							<td>
								{{ $contact->description }}
							</td>
							<td>
								{{ $contact->user_id }}
							</td>
							<td class="actions">
							<span class="dropdown">
								<li> Actions </li>
								<span class="dropdown-content_tbl">
									<a href="/edit/{{ $contact->id }}">Edit</a>
									<br/><br/>
									<a href = "/delete/{{ $contact->id }}">Delete</a>
									<br/><br/>
									<a href="/upload/{{ $contact->id }}">Update Photo</a>
								</span>
							</span>
						</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>