<div class="panel panel-primary panel-colorful">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <p class="text-center text-2x">
                    Welcome,
                    @if(Auth::check())
                        {{ Auth::user()->name }}!
                     @else
                        Friend!
                    @endif
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary panel-colorful">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <i class="fa fa-list-ol sizeUp mar-lft"></i>
                    </div>
                    <div class="col-md-7 text-center">
                        <span class="text-4x text-bold">
                            {{ $contactCount }}
                        </span>
                        <span class="text-2x">
                            contacts in total
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary panel-colorful">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <i class="fa fa-list sizeUp mar-lft"></i>
                    </div>
                    <div class="col-md-7 text-center">
                        <span class="text-4x text-bold">
                            {{ $categoryCount }}
                        </span>
                        <span class="text-2x">
                            categories
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-warning panel-colorful">
            <h3 class="panel-title text-center">
                <i class="fa fa-pie-chart sizeUp mar-top"></i>
                Contacts By Category
            </h3>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="categoriesDonut" style="height:400px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>