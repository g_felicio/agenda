<!DOCTYPE html>
<html>
	<head>
		@php
			$contacts=App\Models\Contact::all();
		@endphp

		<meta charset="utf-8">

		<title>
			Phonebook - All Contacts
		</title>

		<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('css/nifty.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('plugins/datatables/media/css/dataTables.bootstrap.css')}}" rel="stylesheet">
		<link href="{{URL::asset('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="{{URL::asset('css/dropdownAction.css')}}">
		<link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
	</head>
	<body>
		@include('partials.header')<br/>
		@include('partials.allcontacts')
	</body>
</html>