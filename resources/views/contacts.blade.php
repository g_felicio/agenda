<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<script src="/js/jquery-2.2.1.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/switchery.min.js"></script>
		<script src="/js/bootstrap-select.min.js"></script>
		<script src="/js/jquery.dataTables.js"></script>
		<script src="/js/dataTables.bootstrap.js"></script>
		<script src="/js/dataTables.responsive.min.js"></script>

		<link href="{{URL::asset('/css/bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/nifty.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/switchery.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/bootstrap-select.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/dataTables.bootstrap.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/dataTables.responsive.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/bootstrap-table.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/bootstrap-editable.css')}}" rel="stylesheet">
		<link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="{{URL::asset('css/contactsst.css')}}">
		<link rel="stylesheet" href="{{URL::asset('css/infost.css')}}">
		<link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
		<title> 
			Phonebook - 
			@if(\Auth::check())
				{{ Auth::user()->name }} 
			@endif
			Contacts
		</title>

		<script type="text/javascript">
			$(document).ready(function(){
			    $('#demo-dt-basic').DataTable();
			});
		</script>
	</head>		
	<body>
        @include('partials.header')

		@if(Auth::check())
			<br/><br/>
			@include('partials.contactsTable')
		@else
        	@include('partials.info')
        @endif
	</body>
</html>