<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            Phonebook - Users
        </title>

            @php
                $user = App\Models\User::all();
            @endphp
            
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/x-editable/css/bootstrap-editable.css')}}" rel="stylesheet">
        <link href="{{URL::asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('plugins/datatables/media/css/dataTables.bootstrap.css')}}" rel="stylesheet">
        <link href="{{URL::asset('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/demo/nifty-demo.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('css/usersst.css')}}">
    </head>
    <body>
        @include('partials.usersTable')
    </body>
</html>