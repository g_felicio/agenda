<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Phonebook - Register</title>
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
    </head>
    <body>
        @include('partials.header')
        <br/><br/>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">
                                REGISTER
                            </h3>
                        </div>
                        <div class="panel-body">
                            @include('partials.userRegister')
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </body>
</html>