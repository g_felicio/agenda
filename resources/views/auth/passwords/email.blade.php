<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <title>
            Reset Password
        </title>

        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
    </head>
    <body>
        @include('partials.header')<br/>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-warning panel-bordered">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Reset Password
                            </h3>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('partials.resetUserEmail')
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>