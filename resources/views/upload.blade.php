<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<title>
			Phonebook - Upload Contact Image
		</title>	

		<link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/header.css')}}" rel="stylesheet">
	</head>
	<body>		
		@include('partials.header')<br/>
		
		<div class="container">
			@include('partials.uploadContactImage')
		</div>
	</body>
</html>