<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<title>
			Phonebook - Account Deleted
		</title>
		
		<link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
		<link rel="stylesheet" href="{{URL::asset('css/delsst.css')}}">
	</head>
	<body>
		@include('partials.header')
			
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					@include('partials.delsMessage')
				</div>
			</div>
		</div>
	</body>
</html>