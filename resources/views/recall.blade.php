<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		
		<title>
			Phonebook - Delete Account
		</title>
		
		<link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/recallst.css')}}">
	</head>
	<body>
		@include('partials.header')
		<br/>
		{{-- rule of thumb: always start pages with a container div --}} 
		<div class="container">
        	@include('partials.userRecall')
        </div>
    </body>
</html>