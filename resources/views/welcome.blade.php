<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<script src="js/jquery-2.2.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/fastclick.min.js"></script>
		<script src="js/nifty.min.js"></script>
		<script src="js/switchery.min.js"></script>
		<script src="js/bootstrap-select.min.js"></script>
		<script src="js/morris.min.js"></script>
    	<script src="js/raphael.min.js"></script>
    	<script src="js/jquery.sparkline.min.js"></script>
    	<script src="js/jquery.flot.min.js"></script>
    	<script src="js/jquery.flot.resize.min.js"></script>
    	<script src="js/jquery.flot.pie.min.js"></script>
    	<script src="js/gauge.min.js"></script>
    	<script src="js/jquery.easypiechart.min.js"></script>
		<script src="js/nifty-demo.min.js"></script>
		<script src="js/charts.js"></script>

		<link href="{{URL::asset('docs/nifty-v2.4/demo/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/css/nifty.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('docs/nifty-v2.4/demo/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/switchery.min.css')}}" rel="stylesheet"> 
        <link href="{{URL::asset('css/bootstrap-select.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/morris.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('/css/bgColor.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/welcome.css')}}">
        
		<title>
			Welcome,
			@if(\Auth::check())
				{{Auth::user()->name}}
			@else
				{{'Friend'}}
			@endif
		</title>
	</head>
	<body>
		@include('partials.header')
		<br/>
		<div class="container">
    		<div class="row">
		        <div class="col-md-12">
		            @include('partials.welcomePanel')
		        </div>
    		</div>
		</div>

		<script type="text/javascript">
			Morris.Donut({
				element: 'categoriesDonut',
				data: [
					@foreach($contactsByCategory as $category => $contacts)
						{label: "{{ $category }}", value: {{ $contacts->count() }} },
					@endforeach
				]
			});
		</script>
	</body>
</html>

