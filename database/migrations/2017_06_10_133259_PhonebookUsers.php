<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhonebookUsers extends Migration
{
    protected $primaryKey = 'name';
    public $incrementing = false;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) 
        {
            $table->engine = 'InnoDB';

            $table->integer('id')->unsigned()->autoIncrement();
            $table->string('user_image');
            $table->string('name');
            $table->string('email', 191)->unique();
            $table->string('password', 191);
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
            $table->rememberToken();
        });

    }

        // 2017_07_10_133259  THEN 2017_06_...changed to
        // 2017_05_26_120607 changed from
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
