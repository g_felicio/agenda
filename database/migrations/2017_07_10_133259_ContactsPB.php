<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactsPB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) 
        {
            $table->engine = 'InnoDB';

            $table->integer('id')->unsigned()->autoIncrement();
            $table->integer('user_id')->unsigned();            
            $table->string('image');
            $table->string('name');
            $table->string('lastname');
            $table->string('email', 191);
            $table->string('phone',191);
            $table->string('address');
            $table->string('description', 255);
            $table->integer('category_id')->unsigned()->nullable();
            // $table->string('category')->nullable();
            // $table->foreign('category')->references('category')->on('categories');
        });

        Schema::table('contacts', function (Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
            // $table->foreign('category')->references('category')->on('categories');
        });
    }
        // 2017_07_10_133259 change to
        // 2017_05_31_133835 changed from
   

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
