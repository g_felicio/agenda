<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_user', function (Blueprint $table) 
        {
            $table->engine = 'InnoDB';

            $table->integer('contacts_id')->unsigned();
            $table->foreign('contacts_id')->references('id')->on('contacts');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    // changed to 2017_07_10_133259 133359
    
    // changed from  2017_06_19_154048 133259

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_user');
    }
}
