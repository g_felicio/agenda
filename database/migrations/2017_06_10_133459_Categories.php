<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Category;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) 
        {
            $table->engine = 'InnoDB';

            $table->integer('id')->unsigned()->autoIncrement();
            $table->string('category')->required()->unique()->index();
            $table->integer('user_id')->unsigned()->nullable()->onDelete('cascade');
        });

        Schema::table('categories', function (Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users');
        });

        DB::table('categories')->insert(
            array(
                'id' => '1',
                'category' => 'Self'
            )
        );
    }
        // 2017_07_10_133259 changed from
        // 2017_06_10_133459 changed to
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
