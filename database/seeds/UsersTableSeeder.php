<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Database\Factories\ModelFactory;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
       $faker=Faker::create();

       $limit = 5;

       for($i = 0; $i < $limit; $i++)
       {
            DB::table('contacts')->insert([ //,
                'user_id'     => '1',
                'name'        => $faker->firstname,
                'lastname'    => $faker->lastname,
                'email'       => $faker->unique()->email,
                'phone'       => $faker->unique()->phoneNumber,
                'address'     => $faker->address,
                'description' => $faker->word,
                'category_id' => '3',
            ]);
       }

        //  INSERÇÃO MANUAL DE DADOS NA TABELA 'USERS', NO BANCO DE DADOS 'CONTACTSPB'.
        // DB::table('contacts')->insert
        // (
        //     [
        //         'user_id'=>'1',
        //         'name'=>'Gerald',
        //         'lastname'=>'Zed',
        //         'email'=>'g.zed@gmail.com',
        //         'phone'=>'11998772633',
        //         'address'=>'Rua José da Silva, 1438, Vila Madalena, São Paulo, SP, Brasil',
        //         'description'=> 'Work',
        //         'category_id'=>'2',
        //     ]

            // [
            //     'name'=>'José',
            //     'lastname'=>'Cerqueira',
            //     'email'=>'jo.cer@gmail.com',
            //     'phone'=>'11998749023',
            //     'address'=>'Rua Japão, 38, Jardim das Rosas, São Paulo, SP, Brasil',
            //     'description'=> 'Work',
            // ]

            // [
            //     'name'=>'Celso',
            //     'lastname'=>'Silva',
            //     'email'=>'c.silva@gmail.com',
            //     'phone'=>'16993841742',
            //     'address'=>'Rua José da Silva, 18, Bairro São Sebastião, Franca, SP, Brasil',
            //     'description'=> 'Work',
            // ]
        // );
    }
}
