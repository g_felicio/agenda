<?php

namespace App\Traits;

use InfyOm\Generator\Utils\ResponseUtil;

trait ResponseHelpers {

    /**
     * Success json response helper
     *
     * @param $result
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result, $message)
    {
        return response()->json(ResponseUtil::makeResponse($message, $result));
    }

    /**
     * Error json response helper
     *
     * @param $message
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($message, array $data = [], $status = 200)
    {
        return response()->json(ResponseUtil::makeError($message, $data), $status);
    }
}
