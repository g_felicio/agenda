<?php

namespace App\Traits;

use Requests;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

trait LoginWebService
{

    // protected $URL = 'http://201.48.211.36/csweb/rest/loginService';
    // protected $HEADER = ['token' => 230700];

    /**
     * Make login in webservice
     *
     * @param out AuthenticateRequest $request
     *
     * @return mixed
     */
    public function login(Request $request)
    {
        $headerErrorApi = ['Content-type' => 'application/json'];
        $r = Requests::post($this->URL, $this->HEADER, json_encode($request->all()));

        $body = json_decode($r->body);

        if (is_null($body)) {
            throw new HttpException(500, 'An unexpected error ocurred. Try again later.', null, $request->wantsJson() ? $headerErrorApi : []);
        }

        if ($body->error) {
            throw new HttpException(401, 'Login and/or password invalid.', null, $request->wantsJson() ? $headerErrorApi : []);
        }

        $request->merge((array) $body->data);

        return true;
    }
}
