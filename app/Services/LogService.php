<?php

namespace App\Services;

use Exception;
use Carbon\Carbon;
use App\Models\Log;
use App\Models\Player;
use App\Events\LogCreated;
use Illuminate\Http\Request;
use App\Models\MonitorPlayer;
use Illuminate\Routing\Router;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\Paginator;
use App\Repositories\Contracts\LogRepository;
use App\Repositories\Contracts\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repositories\Contracts\PlayerRepository;
use App\Repositories\Criteria\FromAuthPlayerCriteria;

class LogService
{
    /** @var UserRepository */
    private $userRepository;

    /** @var LogRepository */
    private $logRepository;

    /** @var PlayerRepository */
    private $playerRepository;

    /**
     * @param LogRepository $logRepository
     * @param PlayerRepository $playerRepository
     */
    public function __construct(
        LogRepository $logRepository,
        UserRepository $userRepository,
        PlayerRepository $playerRepository
    ) {
        $this->logRepository = $logRepository;
        $this->userRepository = $userRepository;
        $this->playerRepository = $playerRepository;
    }

    /**
     * Filter the logs within the request fields
     *
     * @param Request $request
     *
     * @return Player|null
     */
    public function search(Request $request)
    {
        $inputs = $request->except([
            '_token',
            '_method',
            'page',
            'entityName',
            'action',
            'apiUrl',
            'startDateTime',
            'endDateTime'
        ]);

        // Remove the attributes in the request that has null or empty values
        $inputs = array_filter($inputs, function ($value) {
            return !is_null($value) && $value !== '';
        });

        if ($request->has('entityName')) {
            array_push($inputs, ['entityName', 'LIKE', "%{$request->entityName}%"]);
        }

        if ($request->has('action')) {
            array_push($inputs, ['action', 'LIKE', "%{$request->action}%"]);
        }

        if ($request->has('apiUrl')) {
            array_push($inputs, ['apiUrl', 'LIKE', "%{$request->apiUrl}%"]);
        }

        if ($request->has('startDateTime')) {
            $startDateTime = Carbon::createFromFormat('d/m/Y H:i', $request->startDateTime);
            array_push($inputs, ['created_at', '>=', $startDateTime]);
        }

        if ($request->has('endDateTime')) {
            $endDateTime = Carbon::createFromFormat('d/m/Y H:i', $request->endDateTime);
            array_push($inputs, ['created_at', '<=', $endDateTime]);
        }

        $logs = $this->logRepository
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->findWhere($inputs);

        return $logs;
    }

    /**
     * Make the log of an exception
     *
     * @param  Exception $e
     *
     * @return Log
     */
    public function forException(Exception $e)
    {
        $log = $this->makeLog('ERROR');
        $log['value'] = $this->assembleValue($e);
        $log['description'] = $e->getMessage();

        $log = $this->logRepository->create($log);

        event(new LogCreated($log, 'ERROR'));

        return $log;
    }

    /**
     * Make the log before the response of request
     *
     * @param  Request $request
     *
     * @return Log
     */
    public function beforeResponse(Request $request)
    {
        $log = $this->makeLog();
        $log['value'] = $this->assembleValue($request);

        return $this->logRepository->create($log);
    }

    /**
     * Make the log after the response of the request
     *
     * @param  Response $response
     *
     * @return Log
     */
    public function afterResponse(Response $response)
    {
        $level = 'INFORMATION';
        if (!$response->isSuccessful()) {
            $level = 'ERROR';
        }

        $logLevel = request()->header('X-Log-Level');
        if (!is_null($logLevel)) {
            $level = $logLevel;
        }

        $log = $this->makeLog($level);
        $log['value'] = $this->assembleValue($response);

        return $this->logRepository->create($log);
    }

    /**
     * Make the log on updating customer
     *
     * @param  Request $request
     *
     * @return Log
     */
    public function makeLogUpdateCustomer($customer)
    {
        $value = $customer->getOriginal();

        $idRequester = user() ? user('id') : null;
        if (is_null($idRequester)) {
            $value['login'] = request('user');
        }

        $log = [
            'entityName' => 'Customer',
            'idEntity' => $customer->id,
            'requesterType' => 'user',
            'idRequester' => $idRequester,
            'action' => 'PUT',
            'logType' => 'SERVER',
            'level' => 'INFORMATION',
            'description' => 'BEFORE UPDATE CUSTOMER',
            'value' => json_encode($value)
        ];

        return $this->logRepository->create($log);
    }

    /**
     * Make the log on error when create iuguCustomer
     *
     * @param  $customer
     * @param  $iuguCustomer
     *
     * @return Log
     */
    public function makeLogIuguCustomer($customer, $iuguCustomer, $description)
    {
        $value['iuguCustomer'] = (array) $iuguCustomer;

        $log = [
            'entityName' => 'Customer',
            'idEntity' => $customer->id,
            'requesterType' => 'user',
            'idRequester' => user() ? user('id') : null,
            'action' => 'POST',
            'logType' => 'SERVER',
            'level' => 'ERROR',
            'description' => $description,
            'value' => json_encode($value)
        ];

        return $this->logRepository->create($log);
    }

    /**
     * Make the log on error when create iuguCustomer
     *
     * @param  $customer
     * @param  $iuguCustomer
     *
     * @return Log
     */
    public function makeLogCustomerInactivation($customer, $subscription)
    {
        $value['subscription'] = (array) $subscription;

        $log = [
            'entityName' => 'Customer',
            'idEntity' => $customer->id,
            'requesterType' => 'user',
            'idRequester' => $customer->id,
            'action' => 'POST',
            'logType' => 'APPLICATION',
            'level' => 'INFORMATION',
            'description' => 'CUSTOMER HAS INACTIVATED BY SUBSCRIPTION 30 DAYS SUPENDED',
            'value' => json_encode($value)
        ];

        return $this->logRepository->create($log);
    }

    /**
     * Prepare and return the base of the the log
     *
     * @param  string $level
     *
     * @return array
     */
    private function makeLog($level = 'INFORMATION')
    {
        $log = [];
        $router = app(Router::class);
        $request = $router->getCurrentRequest();
        $currentRoute = $router->getCurrentRoute();

        $actionName = last(explode('\\', $currentRoute->getActionName()));
        $methods = explode('@', $actionName);
        $log['entityName'] = str_replace('Controller', '', head($methods));

        if (!is_null(user())) {
            $log['requesterType'] = 'user';
            $log['idRequester'] = user('id');
        } elseif (!is_null($request->header('player-token'))) {
            $log['requesterType'] = 'player';

            $this->playerRepository->pushCriteria(new FromAuthPlayerCriteria($request));
            $player = $this->playerRepository->first();

            if (!is_null($player)) {
                $log['idRequester'] = $player->id;
            }
        }

        $route = str_plural(strtolower($log['entityName']));

        $log['idEntity'] = $currentRoute->getParameter($route, $request->id);
        $log['level'] = $level;

        $log['description'] = $log['entityName'] . ' ' . last($methods);
        $log['action'] = strtoupper($request->method());
        $log['apiUrl'] = $request->fullUrl();
        $log['logType'] = 'SERVER';

        if (is_array($request->_log)) {
            $log = array_merge($log, $request->_log);
        }

        return $log;
    }

    /**
     * Assemble the request value
     *
     * @param $value
     *
     * @return string
     */
    private function assembleValue($value)
    {
        if ($value instanceof Request) {
            $exceptInputs = array_merge(array_keys($value->file()), ['pass', 'password']);
            return json_encode([
                'files' => $value->file(),
                'inputs' => array_except($value->all(), $exceptInputs),
                'headers' => $value->header(),
            ]);
        }

        if ($value instanceof JsonResponse) {
            return json_encode($value->getData());
        }

        if ($value instanceof Exception) {
            return $value;
        }

        return json_encode($value);
    }

    /**
     * Get the lasts limited created registers from especific player on database
     *
     * @param  int $id
     *
     * @return Collection<Log>
     */
    public function monitorFindByPlayer($id)
    {
        $monitor = $this->logRepository->monitorFindByPlayer($id);

        try {
            $jsonPlayer = json_decode($monitor->value);
            if (!is_object($jsonPlayer)) {
                return null;
            }
            return new MonitorPlayer($jsonPlayer);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Search for requester by qurey passed and requesterType
     * GET /logs/getRequesters
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function getRequesters(Request $request)
    {
        if ($request->requesterType == 'player') {
            $players = $this->playerRepository->findByQuery($request->input('query'));
            return $players;
        }
        $users = $this->userRepository->findByQuery($request->input('query'));
        $users = $users->each(function ($user) {
            $customerName = isset($user->customer) ? ' - ' . $user->customer->tradingName: null;
            $user->desc = $user->login . $customerName;
        });

        return $users;
    }

    /**
     * Store a newly created Log in storage and verify if is necessaery change the level
     * before storage.
     * GET /logs/getRequesters
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function saveMonitor($request)
    {
        //cria uma variavel contendo a request somente para ser possivel alterar
        //o camop level e enviar a request para a criação do log
        $requestChanged = $request->all();
        $requestChanged['level'] = 'ERROR';

        try {
            $jsonPlayer = json_decode($request->value);
            if (!is_object($jsonPlayer)) {
                $logs = $this->logRepository->create($requestChanged);
            }
            $jsonMonitor = new MonitorPlayer($jsonPlayer);
        } catch (Exception $e) {
            $logs = $this->logRepository->create($requestChanged);
        }

        if ($jsonMonitor->version) {
            $this->playerRepository->update(['version' => $jsonMonitor->version], $request->idEntity);
        }

        $requestChanged['level'] = 'INFORMATION';

        //Verifica se possui dados no nó Updates e se a ultima atualização foi a mais de 4 horas.
        if ($jsonMonitor->updates) {
            $dates = $jsonMonitor->updates;
            $last_time_send_logs = Carbon::parse($dates->last_time_send_logs);
            $last_time_update = Carbon::parse($dates->last_time_update);
            $last_version_update = Carbon::parse($dates->last_version_update);

            $dataAtual = Carbon::now();
            if ($last_time_send_logs->diffInHours($dataAtual) > 4
                || $last_time_update->diffInHours($dataAtual)  > 4
                || $last_version_update->diffInHours($dataAtual)  > 4
            ) {
                $requestChanged['level'] = 'ALERT';
            }
        //Verifica se existem arquivos para converter com erro.
        } elseif (count($jsonMonitor->apps->converter->ignore) >= 1) {
            $requestChanged['level'] = 'ERROR';
        //Verifica se o espaço em disco está faltando 20% ou 80% de uso.
        } elseif ($jsonMonitor->system->disk->percent >= 80) {
            $requestChanged['level'] = 'ALERT';
        }

        return $this->logRepository->create($requestChanged);
    }
}
