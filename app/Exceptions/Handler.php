<?php

namespace App\Exceptions;

use Exception;
use App\Services\LogService;
use App\Traits\ResponseHelpers;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    use ResponseHelpers;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);

        if($this->shouldReport($e))
        {
            app(LogService::class)->forException($e);
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if($request->ajax() || $request->wantsJson() && !$request instanceof APIRequest)
        {
            if($e instanceof ModelNotFoundException)
            {
                return $this->sendError($e->getMessage(), [], 404);
            }

            if($e instanceof PlayerTokenNotFound)
            {
                return $this->sendError($e->getMessage(), [], 401);
            }

            if(method_exists($e, 'getResponse'))
            {
                return $e->getResponse();
            }

            $status_code = 500;
            if(method_exists($e, 'getStatusCode'))
            {
                $status_code = $e->getStatusCode();
            }

            return $this->sendError($e->getMessage(), [], $status_code);
        }
        return parent::render($request, $e);
    }
}
