<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	// setting up the relation between contacts and a category
    public function contact()
    {
    	return $this->belongsToMany('App\Models\Contact');
    }

    // setting the relationship between the model and the table
    // basically telling "this is your table" to the model
    protected $table = 'categories';

    // telling that the 'category' column in the 'categories' table
    // should be permeated via a fillable field
    // I guess that's it, at least
    public $fillable = array('category');

    // no timestamps for this table, please
    public $timestamps = false;
}
