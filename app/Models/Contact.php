<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	// Getting the user associated with the contact
    // which means, I hope,
    // The user that created the contact entry in the table 'contacts'
    public function user()
    {
    	return $this->belongsToMany('App\Models\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    protected $table = 'contacts';

    // not using an id field
    // could this be the error?
    // dunno
    // isn't laravel making an automatic id field?
    // dunno too
    // well, id fields aren't fillable, so...
    public $fillable = array('name', 'lastname', 'email', 'phone', 'address', 'description');
    
    //added because of error at migration
    public $timestamps = false;
}