<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'users';
    // protected $primaryKey = 'name';
    // public $incrementing = false;

    // Putting it in simple terms, Hopefully this will return the many contacts
    // that said user recorded in the 'contacts' table
    public function contact()
    {
        return $this->hasMany('App\Models\Contact','contact_user','contacts_id');
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}