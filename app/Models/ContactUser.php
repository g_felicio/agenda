<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUser extends Model
{
    protected $table = 'contact_user';

    public function users()
    {
    	return $this->hasMany('App\Models\User', 'user_id');
    }

    public function contatos()
    {
    	return $this->hasMany('App\Models\Contact', 'contacts_id');
    }  

    public $timestamps = false;
}
