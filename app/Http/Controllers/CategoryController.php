<?php

namespace App\Http\Controllers;

use App;
use App\Models\Contact as Contact;
use App\Models\User as User;
use App\Models\Category as Category;
use DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\CreateCategoryRequest;

use Illuminate\Support\Facades\Input;
use Validator;

use Illuminate\Database\Eloquent\Relations;

class CategoryController extends Controller
{
    public function create(CreateCategoryRequest $request)
    {
        $category           = new Category;
        $category->category = $request->get('category');
        $category->user_id  = \Auth::user()->id;

        $category->save();
        
        // return redirect(route('phonebook', compact('categories')))->with($category);
        return $category;
    }
}
