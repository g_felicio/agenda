<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use App\Models\User as User;
use App\Models\Contact as Contact;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests\UserCreateRequest;

class UserController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $redirectAfterLogout = '/login';

    protected $guard = 'admin';

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateUserRequest $request)
    {
        $user            = new User;
        $user-> name     = $request->get('name');
        $user-> email    = $request->get('email');
        $user-> password = bcrypt($request->get('password'));
                
        // Checking if input has a file
        if($request->hasFile('user_image'))
        {            
            // It does, so set name to a random string with 10 chars
            $userFileName = str_random(10);
                    
            // Get the extension of the file (.jpg, .png...)
            $userFileName .= '.' . $request->file('user_image')->getClientOriginalExtension();
                    
            // Move the file to the storage
            $request->file('user_image')->move(storage_path('app/public/user'), $userFileName);

            // Attribute the contact to the image and then to the name of the file
            // one thing "pulling" another
            $user->user_image = $userFileName;
        }

        $user->save();
    
        return view('/home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \Auth::user()->id;

        return view('reform', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $user          = User::findOrFail(\Auth::user()->id);
        $user -> name  = Input::get('name');
        $user -> email = Input::get('email');

        if (Input::hasFile('user_image')) 
        {
            $userFileName = str_random(10);
            
            $userFileName .= '.' . Input::file('user_image')->getClientOriginalExtension();
            
            Input::file('user_image')->move(storage_path('app/public/user'), $userFileName);

            $user->user_image = $userFileName;
        }

        $user->save();

        return redirect(route('home'));
    }

    public function recall()
    {
        return view('recall');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $contact = Contact::where('user_id', \Auth::user()->id)->delete();

        $user->delete();

        return view('dels');
    }
}
