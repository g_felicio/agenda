<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Utils\ResponseUtil;

use Response;

use App\Traits\ResponseHelpers;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
	use ResponseHelpers;
	
	protected function logAttributes(array $attributes)
	{
		app('request')->merge(['_log' => $attributes]);
	}

    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }
}
