<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ContactUser as ContactUser;
use App\Models\Contact as Contact;

class ContactUserController extends Controller
{
    public function create($id)
    {
    	$contactUser 			  = new ContactUser;
    	$contactUser->contacts_id = Contact::findOrFail($id);
    	$contactUser->user_id 	  = \Auth::user()->id;

    	$contactUser->save();
    }
}
