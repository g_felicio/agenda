<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Models\User as User;
use App\Models\Contact as Contact;
use App\Models\Category as Category;

use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();

        $categoryCount = Category::all()->count();
        $contactCount  = Contact::where('user_id', \Auth::user()->id)->count();

        $contactsAuthUser = Contact::where('user_id', \Auth::user()->id)->get();
        $contactsByCategory = $contactsAuthUser->groupBy('category.category');

        return view('/home', compact('user', 'categoryCount', 'contactCount', 'contactsByCategory'));
    }
}
