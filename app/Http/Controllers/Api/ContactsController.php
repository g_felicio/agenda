<?php

namespace App\Http\Controllers\Api;

use App;

use App\Models\Contact as Contact;
use App\Models\User as User;
use App\Models\Category as Category;

use DB;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;

use App\Http\Controllers\Auth;
use App\Http\Controllers\Api\AuthController;

use App\Http\Requests\Api\CreateContactRequest;
use App\Http\Requests\StoreContactRequest;

use Illuminate\Support\Facades\Input;
use Validator;

class ContactsController extends AppBaseController
{
    public function welcome()
    {
        $user = \Auth::user();

        $contactCount = Contact::all()->count();
        $categoryCount = Category::all()->count();

        $contactsSelection = Contact::all();
        $contactsByCategory = $contactsSelection->groupBy('category.category');
        
        return view('/welcome', compact('user', 'contactCount', 'categoryCount', 'contactsByCategory'));
    }

    // showing the form in the phonebook.blade.php
    // form is structured using html and stuff
    public function index()
    {
        $categories = App\Models\Category::all();
        return view('phonebook', compact('categories'));
    }

    // showing the table where I save my contacts
    // simple view stuff
    public function tab()
    {
        // if the user is registered...
        if(\Auth::check())
        {
            // pass the parameters to permeate its contacts table...
            $contacts = Contact::where('user_id', \Auth::user()->id)->get();
            // that is generated in this view
            return view('contacts', compact('contacts'));
        }
        else
        {
            // if there is no user registered, return the same view, but with a restraining message
            return view('contacts', compact('contacts'));
        }
    }

    // the parameters to save my contact, with errors and stuff, working since day one
    // passed the errors, messages and validation params to CreateContactRequest
    public function create(CreateContactRequest $request)
    {
        $contact               = new Contact;
        $contact-> name        = $request->get('name');
        $contact-> lastname    = $request->get('lastname');
        $contact-> email       = $request->get('email');
        $contact-> phone       = $request->get('phone');
        $contact-> address     = $request->get('address');
        $contact-> description = $request->get('description');
        $contact-> user_id     = \Auth::user()->id;
        $contact-> category_id = $request->get('category');
                
        // Checking if input has a file
        if ($request->hasFile('image'))
        {            
            // It does, so set name to a random string with 10 chars
            $fileName = str_random(10);
            
            // Get the extension of the file (.jpg, .png...)
            $fileName .= '.' . $request->file('image')->getClientOriginalExtension();
            
            // Move the file to the storage
            $request->file('image')->move(storage_path('app/public/uploads'), $fileName);

            // Attribute the contact to the image and then to the name of the file
            // one thing "pulling" another
            $contact->image = $fileName;
        }
        // everything worked? it's saving time
        $contact->save();
        // passing the params to permeate the contacts table
        $contacts = Contact::where('user_id', \Auth::user()->id)->get();
        // returning the contacts view with a clean url
        return redirect(route('contacts', compact('contacts')));
    }
    
    // editing a contact, taking all the info that is in my contacts table according to the id in that row    
    // permeating an edit form similar to the one in the main form using the edit.blade.php
    public function edit($id)
    {
        $contact = Contact::where('user_id', \Auth::user()->id)->findOrFail($id);

        return view('edit', compact('contact'));
    }

    // Create the update request for the table
    public function update($id)
    {
        $contact                = Contact::findOrFail($id);
        $contact -> name        = Input::get('name');
        $contact -> lastname    = Input::get('lastname');
        $contact -> email       = Input::get('email');
        $contact -> phone       = Input::get('phone');
        $contact -> address     = Input::get('address');
        $contact -> description = Input::get('description');
        // made some editing and all, then it's time to save
        $contact -> save();
        // passing params to permeate contacts table
        $contacts = Contact::where('user_id', \Auth::user()->id)->get();
        // getting the contacts view with a clear url
        return redirect(route('contacts', compact('contacts')));
    }

    // confirm contact deletion before actually deleting it
    public function confirmDel($id)
    {
        // take the contact's prameter, like name, lastname and so on
        $contact = Contact::findOrFail($id);
        // permeate the deleteconfirm view with this info
        return view('deleteconfirm', compact('contact'));
    }
    
    // deleting an input in my table, called with a link, going to delete.blade.php.
    // the function is called when loading the delete.blade.php
    public function destroy($id)
    {
        // finding the id of my contact and deleting
        // everything under this id which means
        // my entire contact info
        $contact = Contact::where('user_id', \Auth::user()->id)->findOrFail($id);
        $contact->delete();
        
        // this will return the delete view, which is the confirmation that the code works
        return view('delete');
    }

    // tests about uploading photos. create a button or link in the table to redirect to the upload page    
    // select a pic using an input form, then use the move method to store my pic in a directory
    // but let's begin with simplicity let's just go to the form that'll request the photo
    public function upload($id)
    {
        // simply return the view upload.blade.php, that has the form to input my pic,
        // using the params to permeate it
        $contact = Contact::where('user_id', \Auth::user()->id)->findOrFail($id);
        // that's the view, dude
        return view('upload', compact('contact'));
    }

    // moving the file to a dir
    public function move(Request $request, $id)
    {
        // check if the input has a file
        if ($request->hasFile('image')) {

            // if it does, create a name for the file, a random string with 10 characters (default 40)            
            $fileName = str_random(10);

            // take this name we just created and add the file's extension using the original extension             
            //adiciona extensão da imagem no nome do arquivo
            $fileName .= '.' . $request->file('image')->getClientOriginalExtension();

            // save image to the right diretory
            // Salva a imagem no diretorio selecionado
            $request->file('image')->move(storage_path('app/public/uploads'), $fileName);

            // Attribute the contact, using its id, to the image uploaded and then to the name of the file
            // like in the create method, one thing pulls the other
            $contact        = Contact::findOrFail($id);
            $contact->image = $fileName;

            // then save the contact with everything attributed
            $contact->save();
        }
        // some params editing to permeate the contacts table
        $contacts = Contact::where('user_id', \Auth::user()->id)->get();
        // finally return the view
        return redirect(route('contacts', compact('contacts')));
    }
}