<?php

namespace App\Http\Controllers\Api;

use Requests;
use App\Services\UserService;
use App\Events\Api\AuthEvent;
use App\Traits\LoginWebService;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Api\AuthenticateRequest;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers\Api
 */
class AuthController extends AppBaseController
{
    use LoginWebService;

    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        $this->middleware('log');
    }

    /**
     * Register/login the user with the login and password
     * POST /v1/register
     *
     * @return string $token
     */
    public function authenticate(AuthenticateRequest $request)
    {
        $this->login($request);

        $user = $this->userService->authenticate($request);

        if(is_null($user)) {
            return $this->sendError('Login and/or password incorrect.', [], 401);
        }

        $this->logAttributes([
            'requesterType' => 'user',
            'idRequester' => $user->id
        ]);

        return $this->sendResponse(['token' => $user->api_token], 'User Authenticated successfully');
    }
}
