<?php

use Illuminate\Support\Facades\Input;
use App\Models\Contact;
use App\Models\User;
use App\Models\Category;
use App\Http\Controllers;

// use App\Http\Controllers\Auth;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\PasswordController;
// use Illuminate\Routing\Router;
	/*Vendor\Laravel\Framework\Src\;*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// the landing page when the site is launched
// permeating it with some info
Route::get('/', function () {
	$user = \Auth::user();

    $contactCount = Contact::all()->count();
    $categoryCount = Category::all()->count();

    $contactsSelection = Contact::all();
    $contactsByCategory = $contactsSelection->groupBy('category.category');

    return view('welcome', compact('user', 'contactCount', 'categoryCount', 'contactsByCategory'));
});

// taking the authorization routes
// automatic laravel stuff
Route::auth();

// landing page after user is registered or logged in
// using an automatic laravel controller to do it
Route::get('/home', 'HomeController@index')->name('home');

// getting the resourceful routes from the controller
Route::resource('contacts', 'ContactsController');

// landing route when logging in
// Route::get('welcome', function()
// {
// 	return view('welcome');
// });

// another welcome page landing for when the site is web launched
// don't know why it is repeated, but I'll let it be for now
Route::get('/welcome', 'ContactsController@welcome')->name('welcome');

// getting the form from the controller, wich is in  phonebook.blade.php view
Route::get('phonebook', 'ContactsController@index');

// showing the table in contacts.blade.php view
Route::get('contacts', 'ContactsController@tab')->name('contacts');

// the OLD route to post the contact in the table, using the form in the phonebook.blade.php
// Route::post('contacts', 'ContactsController@create');

// the route to post the contact in the table, using the form in the phonebook.blade.php
Route::post('contacts/create', [
	'as' => 'contacts/create', 'uses' => 'ContactsController@create', 'redirects' => 'contacts'
]);

// route for category creation
Route::post('category/create', [
	'as'=>'category.create', 'uses'=>'CategoryController@create'
]);

// route to contact's delete confirmation
Route::get('deleteconfirm/{id}', [
	'as' => 'deleteconfirm/{id}', 'uses' => 'ContactsController@confirmDel'
]);

// routing to the destroy function in ContactsController
// wich will return the delete.blade.php
Route::get('delete/{id}', [
    'as' => 'delete', 'uses' => 'ContactsController@destroy'
]);

// update working
// using the same id field to permeate our form in edit.blade.php
// using put action in form. the put action updates the stuff
Route::put('update', [
	'as' => 'update', 'uses' => 'ContactsController@edit', 'redirects' => 'contacts'
]);

// getting the edit.blade.php
// permeate the inputs of name, lastname, email, phone, address and description with the same inputs we have in the row of our table 
Route::get('edit/{id}', [
	'as' => 'edit', 'uses' => 'ContactsController@edit'
]);

// getting the upload view according to the id of my contact
Route::get('upload/{id}', [
	'as' => 'upload', 'uses' => 'ContactsController@upload'
]);

// updating the image in the contact info
Route::put('contacts/upload/{id}', [
	'as' => 'contacts.upload', 'uses' => 'ContactsController@move'
]);

// Routes to edit user info
Route::get('reform/{id}', [
	'as' => 'reform', 'uses' => 'UserController@edit'
]);

// The update route itself
Route::put('user/update', [
	'as' => 'user.update', 'uses' => 'UserController@update', 'redirects' => 'home'
]);

// The route to the delete confirmation
Route::get('recall/{id}', [
	'as' => 'recall', 'uses' => 'UserController@recall'
]);

// Use this route to confirm that the user was deleted
Route::get('/dels/{id}', [
	'as' => 'dels', 'uses' => 'UserController@destroy'
]);

// unused view
// this was a view where an user could see all contacts, with abridged information
// it could only see the name and phone, I guess
Route::get('viewallcontacts', function()
{
	return view('viewallcontacts');
});

// users table and all contacts table
// no permissions required to view 
// but no write/edit privileges
Route::get('users', function()
{
	return view('users');
});

// a route to see all contacts, with no user privileges
// simple stuff so I could keep away from SQL app
Route::get('allcontacts', function()
{
	return view('allcontacts');
});

// clean routes
// css/bootstrap editing purposes
Route::get('recall', function()
{
	return view('recall');
});

Route::get('dels', function()
{
	return view ('dels');
});

Route::get('delete', function()
{
	return view('delete');
});

Route::get('reform', function()
{
	return view('reform');
});

Route::get('reset', function()
{
	return view('auth/passwords/reset');
});	

Route::get('email', function()
{
	return view('auth/passwords/email');
});