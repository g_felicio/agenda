<?php 

use App\Models\Contact;
use App\Models\User;
use App\Models\Category;

use App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;

// use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\PasswordController;

use App\Http\Controllers\Api;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ContactsController;

/*
|-----------------------------------------------------------------------------------------------------------|
| API Routes    																							|
|-----------------------------------------------------------------------------------------------------------|
|																											|
| Here is where all API routes are defined.																	|
|																											|
|-----------------------------------------------------------------------------------------------------------|
*/

Route::group(['prefix' => 'api'], function ()
{
	Route::group(['middleware' => 'auth:api'], function()
	{
		Route::resource('contacts', 'ContactsController');

		// the landing page when the site is launched
		// permeating it with some info
		Route::get('/', function () 
		{
			$user = \Auth::user();

		    $contactCount = Contact::all()->count();
		    $categoryCount = Category::all()->count();

		    $contactsSelection = Contact::all();
		    $contactsByCategory = $contactsSelection->groupBy('category.category');

		    return view('welcome', compact('user', 'contactCount', 'categoryCount', 'contactsByCategory'));
		});

		// another welcome page landing for when the site is web launched
		// don't know why it is repeated, but I'll let it be for now
		Route::get('/welcome', 'ContactsController@welcome')->name('welcome');

		// getting the form from the controller, wich is in  phonebook.blade.php view
		Route::get('phonebook', 'ContactsController@index');

		// showing the table in contacts.blade.php view
		Route::get('contacts', 'ContactsController@tab')->name('contacts');

		// the route to post the contact in the table, using the form in the phonebook.blade.php
		Route::post('contacts/create', [
			'as' => 'contacts/create', 'uses' => 'ContactsController@create', 'redirects' => 'contacts'
		]);

		// getting the edit.blade.php
		// permeate the inputs of name, lastname, email, phone, address and description with the same inputs we have in the row of our table 
		Route::get('edit/{id}', [
		'as' => 'edit', 'uses' => 'ContactsController@edit'
		]);

		// update working
		// using the same id field to permeate our form in edit.blade.php
		// using put action in form. the put action updates the stuff
		Route::put('update', [
			'as' => 'update', 'uses' => 'ContactsController@edit', 'redirects' => 'contacts'
		]);

		// route to contact's delete confirmation
		Route::get('deleteconfirm/{id}', [
			'as' => 'deleteconfirm/{id}', 'uses' => 'ContactsController@confirmDel'
		]);

		// routing to the destroy function in ContactsController
		// wich will return the delete.blade.php
		Route::get('delete/{id}', [
	    	'as' => 'delete', 'uses' => 'ContactsController@destroy'
		]);

		// getting the upload view according to the id of my contact
		Route::get('upload/{id}', [
			'as' => 'upload', 'uses' => 'ContactsController@upload'
		]);

		// updating the image in the contact info
		Route::put('contacts/upload/{id}', [
			'as' => 'contacts.upload', 'uses' => 'ContactsController@move'
		]);
	});
}