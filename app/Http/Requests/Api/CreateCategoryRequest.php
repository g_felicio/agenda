<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;
use App\Models\Category;

class CreateCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'string|unique:categories,category|required',
        ];
    }

    public function messages() 
    {
        return [
            'string'   => 'The :attribute must be a string',
            'unique'   => 'The :attribute must be unique. It means it can not repeat',
            'required' => 'The :attribute can not be null.',
        ];
    }
}
