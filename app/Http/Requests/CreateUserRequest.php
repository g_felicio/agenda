<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_image' => 'file',
            'name'       => 'required|alpha|min:3|max:255',
            'email'      => 'required|unique:users,email|email',
            'password'   => 'required|alpha_num|between:6,100',
        ];
    }

    public function messages()
    {
        return[
            'file'      => 'The :attribute must be a file',
            'required'  => 'The :attribute is really important. How will we identify you?',
            'alpha'     => 'The :attribute must contain only letters.',
            'min'       => 'The :attribute must have at least 3 characters.',
            'max'       => 'The :attribute must have a maximum of 255 letters.',
            'email'     => 'The :attribute must be a valid e-mail format address.',
            'alpha_num' => 'The :attribute must contain only numbers and letters.',
            'between'   => 'The :attribute content must have a lenght between 6 and 100 characters.',
        ];
    }
}
