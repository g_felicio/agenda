<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class CreateContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request. Rule::unique('users')->where(function ($query) {
    $query->where('user_id', Auth::user()->id);
})  
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'       => 'file',
            'name'        => 'min:3|max:255|required',
            'lastname'    => 'min:3|max:255',
            'email'       => 'required|email|unique:contacts,email,NULL,id,user_id,'.\Auth::user()->id,
            'phone'       => 'required|numeric|min:3|unique:contacts,phone,NULL,id,user_id,'.\Auth::user()->id,
            'address'     => 'max:255',
            'description' => 'max:255',
            'category'    => 'required|max:255'
        ];
    }

    public function messages() 
    {
        return [
            'file'     => 'The :attribute must be a file.',
            'alpha'    => 'The :attribute must contain only letters.',
            'max'      => 'The :attribute must have a maximum of 255 letters.',
            'min'      => 'The :attribute must have at least 3 characters.',
            'unique'   => 'This :attribute has already been taken. Enter a new :attribute.',
            'required' => 'The :attribute is really important. We are storing your contact info after all...',
            'email'    => 'The :attribute must be a valid e-mail format address.',
            'numeric'  => 'The :attribute must contain only numbers.',
        ];
    }

}
