/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  'use strict';
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 3)) {
    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4')
  }
}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.7
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: alert.js v3.3.7
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.7'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector === '#' ? [] : selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);

/* ========================================================================
 * Bootstrap: button.js v3.3.7
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.7'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state += 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d).prop(d, true)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d).prop(d, false)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked')) changed = false
        $parent.find('.active').removeClass('active')
        this.$element.addClass('active')
      } else if ($input.prop('type') == 'checkbox') {
        if (($input.prop('checked')) !== this.$element.hasClass('active')) changed = false
        this.$element.toggleClass('active')
      }
      $input.prop('checked', this.$element.hasClass('active'))
      if (changed) $input.trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
      this.$element.toggleClass('active')
    }
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target).closest('.btn')
      Plugin.call($btn, 'toggle')
      if (!($(e.target).is('input[type="radio"], input[type="checkbox"]'))) {
        // Prevent double click on radios, and the double selections (so cancellation) on checkboxes
        e.preventDefault()
        // The target component still receive the focus
        if ($btn.is('input,button')) $btn.trigger('focus')
        else $btn.find('input:visible,button:visible').first().trigger('focus')
      }
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.7
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      = null
    this.sliding     = null
    this.interval    = null
    this.$active     = null
    this.$items      = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION  = '3.3.7'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
                || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that      = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel             = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this   = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: collapse.js v3.3.7
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

/* jshint latedef: false */

+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.7'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.7
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.7'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger($.Event('shown.bs.dropdown', relatedTarget))
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index)                                    index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);

/* ========================================================================
 * Bootstrap: modal.js v3.3.7
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.7'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (document !== e.target &&
            this.$element[0] !== e.target &&
            !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tooltip.js v3.3.7
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.7'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      if (that.$element) { // TODO: Check whether guarding this code with this `if` is really necessary.
        that.$element
          .removeAttr('aria-describedby')
          .trigger('hidden.bs.' + that.type)
      }
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var isSvg = window.SVGElement && el instanceof window.SVGElement
    // Avoid using $.offset() on SVGs since it gives incorrect results in jQuery 3.
    // See https://github.com/twbs/bootstrap/issues/20280
    var elOffset  = isBody ? { top: 0, left: 0 } : (isSvg ? null : $element.offset())
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
      that.$element = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: popover.js v3.3.7
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.7'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.7
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.7'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.7
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.7'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);

/* ========================================================================
 * Bootstrap: affix.js v3.3.7
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.7'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);

!function(e){return"function"==typeof define&&define.amd?define(["jquery"],function(t){return e(t,window,document)}):"object"==typeof exports?module.exports=e(require("jquery"),window,document):e(jQuery,window,document)}(function(e,t,n){"use strict";var i,a,o,s,r,l,f,c,d,h,u,g,v,m,p,y,C,k,x,$,b,w,T,O,S,A,N,I,E,P,j;T={paneClass:"nano-pane",sliderClass:"nano-slider",contentClass:"nano-content",iOSNativeScrolling:!1,preventPageScrolling:!1,disableResize:!1,alwaysVisible:!1,flashDelay:1500,sliderMinHeight:20,sliderMaxHeight:null,documentContext:null,windowContext:null},k="scrollbar",C="scroll",d="mousedown",h="mouseenter",u="mousemove",v="mousewheel",g="mouseup",y="resize",r="drag",l="enter",$="up",p="panedown",o="DOMMouseScroll",s="down",b="wheel",f="keydown",c="keyup",x="touchmove",i="Microsoft Internet Explorer"===t.navigator.appName&&/msie 7./i.test(t.navigator.appVersion)&&t.ActiveXObject,a=null,N=t.requestAnimationFrame,w=t.cancelAnimationFrame,E=n.createElement("div").style,j=function(){var e,t,n,i,a,o;for(i=["t","webkitT","MozT","msT","OT"],e=a=0,o=i.length;o>a;e=++a)if(n=i[e],t=i[e]+"ransform",t in E)return i[e].substr(0,i[e].length-1);return!1}(),P=function(e){return j===!1?!1:""===j?e:j+e.charAt(0).toUpperCase()+e.substr(1)},I=P("transform"),S=I!==!1,O=function(){var e,t,i;return e=n.createElement("div"),t=e.style,t.position="absolute",t.width="100px",t.height="100px",t.overflow=C,t.top="-9999px",n.body.appendChild(e),i=e.offsetWidth-e.clientWidth,n.body.removeChild(e),i},A=function(){var e,n,i;return n=t.navigator.userAgent,(e=/(?=.+Mac OS X)(?=.+Firefox)/.test(n))?(i=/Firefox\/\d{2}\./.exec(n),i&&(i=i[0].replace(/\D+/g,"")),e&&+i>23):!1},m=function(){function f(i,o){this.el=i,this.options=o,a||(a=O()),this.$el=e(this.el),this.doc=e(this.options.documentContext||n),this.win=e(this.options.windowContext||t),this.body=this.doc.find("body"),this.$content=this.$el.children("."+this.options.contentClass),this.$content.attr("tabindex",this.options.tabIndex||0),this.content=this.$content[0],this.previousPosition=0,this.options.iOSNativeScrolling&&null!=this.el.style.WebkitOverflowScrolling?this.nativeScrolling():this.generate(),this.createEvents(),this.addEvents(),this.reset()}return f.prototype.preventScrolling=function(e,t){if(this.isActive)if(e.type===o)(t===s&&e.originalEvent.detail>0||t===$&&e.originalEvent.detail<0)&&e.preventDefault();else if(e.type===v){if(!e.originalEvent||!e.originalEvent.wheelDelta)return;(t===s&&e.originalEvent.wheelDelta<0||t===$&&e.originalEvent.wheelDelta>0)&&e.preventDefault()}},f.prototype.nativeScrolling=function(){this.$content.css({WebkitOverflowScrolling:"touch"}),this.iOSNativeScrolling=!0,this.isActive=!0},f.prototype.updateScrollValues=function(){var e,t;e=this.content,this.maxScrollTop=e.scrollHeight-e.clientHeight,this.prevScrollTop=this.contentScrollTop||0,this.contentScrollTop=e.scrollTop,t=this.contentScrollTop>this.previousPosition?"down":this.contentScrollTop<this.previousPosition?"up":"same",this.previousPosition=this.contentScrollTop,"same"!==t&&this.$el.trigger("update",{position:this.contentScrollTop,maximum:this.maxScrollTop,direction:t}),this.iOSNativeScrolling||(this.maxSliderTop=this.paneHeight-this.sliderHeight,this.sliderTop=0===this.maxScrollTop?0:this.contentScrollTop*this.maxSliderTop/this.maxScrollTop)},f.prototype.setOnScrollStyles=function(){var e;S?(e={},e[I]="translate(0, "+this.sliderTop+"px)"):e={top:this.sliderTop},N?(w&&this.scrollRAF&&w(this.scrollRAF),this.scrollRAF=N(function(t){return function(){return t.scrollRAF=null,t.slider.css(e)}}(this))):this.slider.css(e)},f.prototype.createEvents=function(){this.events={down:function(e){return function(t){return e.isBeingDragged=!0,e.offsetY=t.pageY-e.slider.offset().top,e.slider.is(t.target)||(e.offsetY=0),e.pane.addClass("active"),e.doc.bind(u,e.events[r]).bind(g,e.events[$]),e.body.bind(h,e.events[l]),!1}}(this),drag:function(e){return function(t){return e.sliderY=t.pageY-e.$el.offset().top-e.paneTop-(e.offsetY||.5*e.sliderHeight),e.scroll(),e.contentScrollTop>=e.maxScrollTop&&e.prevScrollTop!==e.maxScrollTop?e.$el.trigger("scrollend"):0===e.contentScrollTop&&0!==e.prevScrollTop&&e.$el.trigger("scrolltop"),!1}}(this),up:function(e){return function(){return e.isBeingDragged=!1,e.pane.removeClass("active"),e.doc.unbind(u,e.events[r]).unbind(g,e.events[$]),e.body.unbind(h,e.events[l]),!1}}(this),resize:function(e){return function(){e.reset()}}(this),panedown:function(e){return function(t){return e.sliderY=(t.offsetY||t.originalEvent.layerY)-.5*e.sliderHeight,e.scroll(),e.events.down(t),!1}}(this),scroll:function(e){return function(t){e.updateScrollValues(),e.isBeingDragged||(e.iOSNativeScrolling||(e.sliderY=e.sliderTop,e.setOnScrollStyles()),null!=t&&(e.contentScrollTop>=e.maxScrollTop?(e.options.preventPageScrolling&&e.preventScrolling(t,s),e.prevScrollTop!==e.maxScrollTop&&e.$el.trigger("scrollend")):0===e.contentScrollTop&&(e.options.preventPageScrolling&&e.preventScrolling(t,$),0!==e.prevScrollTop&&e.$el.trigger("scrolltop"))))}}(this),wheel:function(e){return function(t){var n;return null!=t?(n=t.delta||t.wheelDelta||t.originalEvent&&t.originalEvent.wheelDelta||-t.detail||t.originalEvent&&-t.originalEvent.detail,n&&(e.sliderY+=-n/3),e.scroll(),!1):void 0}}(this),enter:function(e){return function(t){var n;return e.isBeingDragged&&1!==(t.buttons||t.which)?(n=e.events)[$].apply(n,arguments):void 0}}(this)}},f.prototype.addEvents=function(){var e;this.removeEvents(),e=this.events,this.options.disableResize||this.win.bind(y,e[y]),this.iOSNativeScrolling||(this.slider.bind(d,e[s]),this.pane.bind(d,e[p]).bind(""+v+" "+o,e[b])),this.$content.bind(""+C+" "+v+" "+o+" "+x,e[C])},f.prototype.removeEvents=function(){var e;e=this.events,this.win.unbind(y,e[y]),this.iOSNativeScrolling||(this.slider.unbind(),this.pane.unbind()),this.$content.unbind(""+C+" "+v+" "+o+" "+x,e[C])},f.prototype.generate=function(){var e,n,i,o,s,r,l;return o=this.options,r=o.paneClass,l=o.sliderClass,e=o.contentClass,(s=this.$el.children("."+r)).length||s.children("."+l).length||this.$el.append('<div class="'+r+'"><div class="'+l+'" /></div>'),this.pane=this.$el.children("."+r),this.slider=this.pane.find("."+l),0===a&&A()?(i=t.getComputedStyle(this.content,null).getPropertyValue("padding-right").replace(/[^0-9.]+/g,""),n={right:-14,paddingRight:+i+14}):a&&(n={right:-a},this.$el.addClass("has-scrollbar")),null!=n&&this.$content.css(n),this},f.prototype.restore=function(){this.stopped=!1,this.iOSNativeScrolling||this.pane.show(),this.addEvents()},f.prototype.reset=function(){var e,t,n,o,s,r,l,f,c,d,h,u;return this.iOSNativeScrolling?void(this.contentHeight=this.content.scrollHeight):(this.$el.find("."+this.options.paneClass).length||this.generate().stop(),this.stopped&&this.restore(),e=this.content,o=e.style,s=o.overflowY,i&&this.$content.css({height:this.$content.height()}),t=e.scrollHeight+a,d=parseInt(this.$el.css("max-height"),10),d>0&&(this.$el.height(""),this.$el.height(e.scrollHeight>d?d:e.scrollHeight)),l=this.pane.outerHeight(!1),c=parseInt(this.pane.css("top"),10),r=parseInt(this.pane.css("bottom"),10),f=l+c+r,u=Math.round(f/t*l),u<this.options.sliderMinHeight?u=this.options.sliderMinHeight:null!=this.options.sliderMaxHeight&&u>this.options.sliderMaxHeight&&(u=this.options.sliderMaxHeight),s===C&&o.overflowX!==C&&(u+=a),this.maxSliderTop=f-u,this.contentHeight=t,this.paneHeight=l,this.paneOuterHeight=f,this.sliderHeight=u,this.paneTop=c,this.slider.height(u),this.events.scroll(),this.pane.show(),this.isActive=!0,e.scrollHeight===e.clientHeight||this.pane.outerHeight(!0)>=e.scrollHeight&&s!==C?(this.pane.hide(),this.isActive=!1):this.el.clientHeight===e.scrollHeight&&s===C?this.slider.hide():this.slider.show(),this.pane.css({opacity:this.options.alwaysVisible?1:"",visibility:this.options.alwaysVisible?"visible":""}),n=this.$content.css("position"),("static"===n||"relative"===n)&&(h=parseInt(this.$content.css("right"),10),h&&this.$content.css({right:"",marginRight:h})),this)},f.prototype.scroll=function(){return this.isActive?(this.sliderY=Math.max(0,this.sliderY),this.sliderY=Math.min(this.maxSliderTop,this.sliderY),this.$content.scrollTop(this.maxScrollTop*this.sliderY/this.maxSliderTop),this.iOSNativeScrolling||(this.updateScrollValues(),this.setOnScrollStyles()),this):void 0},f.prototype.scrollBottom=function(e){return this.isActive?(this.$content.scrollTop(this.contentHeight-this.$content.height()-e).trigger(v),this.stop().restore(),this):void 0},f.prototype.scrollTop=function(e){return this.isActive?(this.$content.scrollTop(+e).trigger(v),this.stop().restore(),this):void 0},f.prototype.scrollTo=function(e){return this.isActive?(this.scrollTop(this.$el.find(e).get(0).offsetTop),this):void 0},f.prototype.stop=function(){return w&&this.scrollRAF&&(w(this.scrollRAF),this.scrollRAF=null),this.stopped=!0,this.removeEvents(),this.iOSNativeScrolling||this.pane.hide(),this},f.prototype.destroy=function(){return this.stopped||this.stop(),!this.iOSNativeScrolling&&this.pane.length&&this.pane.remove(),i&&this.$content.height(""),this.$content.removeAttr("tabindex"),this.$el.hasClass("has-scrollbar")&&(this.$el.removeClass("has-scrollbar"),this.$content.css({right:""})),this},f.prototype.flash=function(){return!this.iOSNativeScrolling&&this.isActive?(this.reset(),this.pane.addClass("flashed"),setTimeout(function(e){return function(){e.pane.removeClass("flashed")}}(this),this.options.flashDelay),this):void 0},f}(),e.fn.nanoScroller=function(t){return this.each(function(){var n,i;if((i=this.nanoscroller)||(n=e.extend({},T,t),this.nanoscroller=i=new m(this,n)),t&&"object"==typeof t){if(e.extend(i.options,t),null!=t.scrollBottom)return i.scrollBottom(t.scrollBottom);if(null!=t.scrollTop)return i.scrollTop(t.scrollTop);if(t.scrollTo)return i.scrollTo(t.scrollTo);if("bottom"===t.scroll)return i.scrollBottom(0);if("top"===t.scroll)return i.scrollTop(0);if(t.scroll&&t.scroll instanceof e)return i.scrollTo(t.scroll);if(t.stop)return i.stop();if(t.destroy)return i.destroy();if(t.flash)return i.flash()}return i.reset()})},e.fn.nanoScroller.Constructor=m}),!function(e,t){"function"==typeof define&&define.amd?define(["jquery"],t):"object"==typeof exports?module.exports=t(require("jquery")):e.sortable=t(e.jQuery)}(this,function(e){"use strict";function t(){var e=document.createElement("mm"),t={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var n in t)if(void 0!==e.style[n])return{end:t[n]};return!1}function n(t){return this.each(function(){var n=e(this),i=n.data("mm"),o=e.extend({},a.DEFAULTS,n.data(),"object"==typeof t&&t);i||n.data("mm",i=new a(this,o)),"string"==typeof t&&i[t]()})}e.fn.emulateTransitionEnd=function(t){var n=!1,a=this;e(this).one("mmTransitionEnd",function(){n=!0});var o=function(){n||e(a).trigger(i.end)};return setTimeout(o,t),this};var i=t();i&&(e.event.special.mmTransitionEnd={bindType:i.end,delegateType:i.end,handle:function(t){return e(t.target).is(this)?t.handleObj.handler.apply(this,arguments):void 0}});var a=function(t,n){this.$element=e(t),this.options=e.extend({},a.DEFAULTS,n),this.transitioning=null,this.init()};a.TRANSITION_DURATION=350,a.DEFAULTS={toggle:!0,doubleTapToGo:!1,preventDefault:!0,activeClass:"active",collapseClass:"collapse",collapseInClass:"in",collapsingClass:"collapsing",onTransitionStart:!1,onTransitionEnd:!1},a.prototype.init=function(){var t=this,n=this.options.activeClass,i=this.options.collapseClass,a=this.options.collapseInClass;this.$element.find("li."+n).has("ul").children("ul").attr("aria-expanded",!0).addClass(i+" "+a),this.$element.find("li").not("."+n).has("ul").children("ul").attr("aria-expanded",!1).addClass(i),this.options.doubleTapToGo&&this.$element.find("li."+n).has("ul").children("a").addClass("doubleTapToGo"),this.$element.find("li").has("ul").children("a").on("click.metisMenu",function(i){var a=e(this),o=a.parent("li"),s=o.children("ul");return t.options.preventDefault&&i.preventDefault(),"true"!==a.attr("aria-disabled")?(o.hasClass(n)&&!t.options.doubleTapToGo?(t.hide(s),a.attr("aria-expanded",!1)):(t.show(s),a.attr("aria-expanded",!0)),t.options.onTransitionStart&&t.options.onTransitionStart(),t.options.doubleTapToGo&&t.doubleTapToGo(a)&&"#"!==a.attr("href")&&""!==a.attr("href")?(i.stopPropagation(),void(document.location=a.attr("href"))):void 0):void 0})},a.prototype.doubleTapToGo=function(e){var t=this.$element;return e.hasClass("doubleTapToGo")?(e.removeClass("doubleTapToGo"),!0):e.parent().children("ul").length?(t.find(".doubleTapToGo").removeClass("doubleTapToGo"),e.addClass("doubleTapToGo"),!1):void 0},a.prototype.show=function(t){var n=this.options.activeClass,o=this.options.collapseClass,s=this.options.collapseInClass,r=this.options.collapsingClass,l=e(t),f=l.parent("li");if(!this.transitioning&&!l.hasClass(s)){f.addClass(n),this.options.toggle&&this.hide(f.siblings().children("ul."+s).attr("aria-expanded",!1)),l.removeClass(o).addClass(r).height(0),this.transitioning=1;var c=function(){this.transitioning&&this.options.onTransitionEnd&&this.options.onTransitionEnd(),l.removeClass(r).addClass(o+" "+s).height("").attr("aria-expanded",!0),this.transitioning=0};return i?void l.one("mmTransitionEnd",e.proxy(c,this)).emulateTransitionEnd(a.TRANSITION_DURATION).height(l[0].scrollHeight):c.call(this)}},a.prototype.hide=function(t){var n=this.options.activeClass,o=this.options.collapseClass,s=this.options.collapseInClass,r=this.options.collapsingClass,l=e(t);if(!this.transitioning&&l.hasClass(s)){l.parent("li").removeClass(n),l.height(l.height())[0].offsetHeight,l.addClass(r).removeClass(o).removeClass(s),this.transitioning=1;var f=function(){this.transitioning&&this.options.onTransitionEnd&&this.options.onTransitionEnd(),this.transitioning=0,l.removeClass(r).addClass(o).attr("aria-expanded",!1)};return i?void l.height(0).one("mmTransitionEnd",e.proxy(f,this)).emulateTransitionEnd(a.TRANSITION_DURATION):f.call(this)}};var o=e.fn.metisMenu;e.fn.metisMenu=n,e.fn.metisMenu.Constructor=a,e.fn.metisMenu.noConflict=function(){return e.fn.metisMenu=o,this}}),!function(e){"use strict";window.nifty={container:e("#container"),contentContainer:e("#content-container"),navbar:e("#navbar"),mainNav:e("#mainnav-container"),aside:e("#aside-container"),footer:e("#footer"),scrollTop:e("#scroll-top"),window:e(window),body:e("body"),bodyHtml:e("body, html"),document:e(document),screenSize:"",isMobile:function(){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)}(),randomInt:function(e,t){return Math.floor(Math.random()*(t-e+1)+e)},transition:function(){var e=document.body||document.documentElement,t=e.style,n=void 0!==t.transition||void 0!==t.WebkitTransition;return n}()},nifty.document.ready(function(){nifty.document.trigger("nifty.ready")}),nifty.document.on("nifty.ready",function(){var t=e(".add-tooltip");t.length&&t.tooltip();var n=e(".add-popover");n.length&&n.popover();var i=e(".nano");i.length&&i.nanoScroller({preventPageScrolling:!0}),e("#navbar-container .navbar-top-links").on("shown.bs.dropdown",".dropdown",function(){e(this).find(".nano").nanoScroller({preventPageScrolling:!0})}),nifty.body.addClass("nifty-ready")})}(jQuery),!function(e){"use strict";var t=null,n=function(e){{var t=e.find(".mega-dropdown-toggle");e.find(".mega-dropdown-menu")}t.on("click",function(t){t.preventDefault(),e.toggleClass("open")})},i={toggle:function(){return this.toggleClass("open"),null},show:function(){return this.addClass("open"),null},hide:function(){return this.removeClass("open"),null}};e.fn.niftyMega=function(t){var a=!1;return this.each(function(){i[t]?a=i[t].apply(e(this).find("input"),Array.prototype.slice.call(arguments,1)):"object"!=typeof t&&t||n(e(this))}),a},nifty.document.on("nifty.ready",function(){t=e(".mega-dropdown"),t.length&&t.niftyMega(),e("html").on("click",function(n){t.length&&(e(n.target).closest(".mega-dropdown").length||t.removeClass("open"))})})}(jQuery),!function(e){"use strict";nifty.document.on("nifty.ready",function(){var t=e('[data-dismiss="panel"]');t.length&&t.one("click",function(t){t.preventDefault();var n=e(this).parents(".panel");n.addClass("remove").on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(e){"opacity"==e.originalEvent.propertyName&&n.remove()})})})}(jQuery),!function(){"use strict";nifty.document.one("nifty.ready",function(){if(nifty.scrollTop.length&&!nifty.isMobile){var e=!0,t=250;nifty.window.scroll(function(){nifty.window.scrollTop()>t&&!e?(nifty.navbar.addClass("shadow"),nifty.scrollTop.addClass("in"),e=!0):nifty.window.scrollTop()<t&&e&&(nifty.navbar.removeClass("shadow"),nifty.scrollTop.removeClass("in"),e=!1)}),nifty.scrollTop.on("click",function(e){e.preventDefault(),nifty.bodyHtml.animate({scrollTop:0},500)})}})}(jQuery),!function(e){"use strict";var t={displayIcon:!0,iconColor:"text-dark",iconClass:"fa fa-refresh fa-spin fa-2x",title:"",desc:""},n=function(){return(65536*(1+Math.random())|0).toString(16).substring(1)},i={show:function(t){var i=e(t.attr("data-target")),a="nifty-overlay-"+n()+n()+"-"+n(),o=e('<div id="'+a+'" class="panel-overlay"></div>');return t.prop("disabled",!0).data("niftyOverlay",a),i.addClass("panel-overlay-wrap"),o.appendTo(i).html(t.data("overlayTemplate")),null},hide:function(t){var n=e(t.attr("data-target")),i=e("#"+t.data("niftyOverlay"));return i.length&&(t.prop("disabled",!1),n.removeClass("panel-overlay-wrap"),i.hide().remove()),null}},a=function(n,i){if(n.data("overlayTemplate"))return null;var a=e.extend({},t,i),o=a.displayIcon?'<span class="panel-overlay-icon '+a.iconColor+'"><i class="'+a.iconClass+'"></i></span>':"";return n.data("overlayTemplate",'<div class="panel-overlay-content pad-all unselectable">'+o+'<h4 class="panel-overlay-title">'+a.title+"</h4><p>"+a.desc+"</p></div>"),null};e.fn.niftyOverlay=function(t){return i[t]?i[t](this):"object"!=typeof t&&t?null:this.each(function(){a(e(this),t)})}}(jQuery),!function(e){"use strict";var t,i,n={},a=!1;e.niftyNoty=function(o){{var c,s={type:"primary",icon:"",title:"",message:"",closeBtn:!0,container:"page",floating:{position:"top-right",animationIn:"jellyIn",animationOut:"fadeOut"},html:null,focus:!0,timer:0,onShow:function(){},onShown:function(){},onHide:function(){},onHidden:function(){}},r=e.extend({},s,o),l=e('<div class="alert-wrap"></div>'),f=function(){var e="";return o&&o.icon&&(e='<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="'+r.icon+'"></i></span></div>'),e},d=function(){var e=r.closeBtn?'<button class="close" type="button"><i class="fa fa-times-circle"></i></button>':"",t='<div class="alert alert-'+r.type+'" role="alert">'+e+'<div class="media">';return r.html?t+r.html+"</div></div>":t+f()+'<div class="media-body"><h4 class="alert-title">'+r.title+'</h4><p class="alert-message">'+r.message+"</p></div></div>"}(),h=function(){return r.onHide(),"floating"===r.container&&r.floating.animationOut&&(l.removeClass(r.floating.animationIn).addClass(r.floating.animationOut),nifty.transition||(r.onHidden(),l.remove())),l.removeClass("in").on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(e){"max-height"==e.originalEvent.propertyName&&(r.onHidden(),l.remove())}),clearInterval(c),null},u=function(e){nifty.bodyHtml.animate({scrollTop:e},300,function(){l.addClass("in")})};!function(){if(r.onShow(),"page"===r.container)t||(t=e('<div id="page-alert"></div>'),nifty.contentContainer.prepend(t)),i=t,r.focus&&u(0);else if("floating"===r.container)n[r.floating.position]||(n[r.floating.position]=e('<div id="floating-'+r.floating.position+'" class="floating-container"></div>'),nifty.container.append(n[r.floating.position])),i=n[r.floating.position],r.floating.animationIn&&l.addClass("in animated "+r.floating.animationIn),r.focus=!1;else{var o=e(r.container),s=o.children(".panel-alert"),f=o.children(".panel-heading");if(!o.length)return a=!1,!1;s.length?i=s:(i=e('<div class="panel-alert"></div>'),f.length?f.after(i):o.prepend(i)),r.focus&&u(o.offset().top-30)}return a=!0,!1}()}if(a){if(i.append(l.html(d)),l.find('[data-dismiss="noty"]').one("click",h),r.closeBtn&&l.find(".close").one("click",h),r.timer>0&&(c=setInterval(h,r.timer)),!r.focus)var v=setInterval(function(){l.addClass("in"),clearInterval(v)},200);l.one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd",function(e){"max-height"!=e.originalEvent.propertyName&&"animationend"!=e.type||!a||(r.onShown(),a=!1)})}}}(jQuery),!function(e){"use strict";var t,n=function(t){if(!t.data("nifty-check")){t.data("nifty-check",!0),t.text().trim().length?t.addClass("form-text"):t.removeClass("form-text");var n=t.find("input")[0],i=n.name,a=function(){return"radio"==n.type&&i?e(".form-radio").not(t).find("input").filter('input[name="'+i+'"]').parent():!1}(),o=function(){"radio"==n.type&&a.length&&a.each(function(){var t=e(this);t.hasClass("active")&&t.trigger("nifty.ch.unchecked"),t.removeClass("active")}),n.checked?t.addClass("active").trigger("nifty.ch.checked"):t.removeClass("active").trigger("nifty.ch.unchecked")};n.checked?t.addClass("active"):t.removeClass("active"),e(n).on("change",o)}},i={isChecked:function(){return this[0].checked},toggle:function(){return this[0].checked=!this[0].checked,this.trigger("change"),null},toggleOn:function(){return this[0].checked||(this[0].checked=!0,this.trigger("change")),null},toggleOff:function(){return this[0].checked&&"checkbox"==this[0].type&&(this[0].checked=!1,this.trigger("change")),null}},a=function(){t=e(".form-checkbox, .form-radio"),t.length&&t.niftyCheck()};e.fn.niftyCheck=function(t){var a=!1;return this.each(function(){i[t]?a=i[t].apply(e(this).find("input"),Array.prototype.slice.call(arguments,1)):"object"!=typeof t&&t||n(e(this))}),a},nifty.document.on("nifty.ready",a).on("change",".form-checkbox, .form-radio",a),nifty.document.on("change",".btn-file :file",function(){var t=e(this),n=t.get(0).files?t.get(0).files.length:1,i=t.val().replace(/\\/g,"/").replace(/.*\//,""),a=function(){try{return t[0].files[0].size}catch(e){return"Nan"}}(),o=function(){if("Nan"==a)return"Unknown";var e=Math.floor(Math.log(a)/Math.log(1024));return 1*(a/Math.pow(1024,e)).toFixed(2)+" "+["B","kB","MB","GB","TB"][e]}();t.trigger("fileselect",[n,i,o])})}(jQuery),!function(e){nifty.document.on("nifty.ready",function(){var t=e("#mainnav-shortcut");t.length&&t.find("li").each(function(){var t=e(this);t.popover({animation:!1,trigger:"hover focus",placement:"bottom",container:"body",template:'<div class="popover mainnav-shortcut"><div class="arrow"></div><div class="popover-content"></div>'})})})}(jQuery),!function(e,t){var n={};n.eventName="resizeEnd",n.delay=250,n.poll=function(){var i=e(this),a=i.data(n.eventName);a.timeoutId&&t.clearTimeout(a.timeoutId),a.timeoutId=t.setTimeout(function(){i.trigger(n.eventName)},n.delay)},e.event.special[n.eventName]={setup:function(){var t=e(this);t.data(n.eventName,{}),t.on("resize",n.poll)},teardown:function(){var i=e(this),a=i.data(n.eventName);a.timeoutId&&t.clearTimeout(a.timeoutId),i.removeData(n.eventName),i.off("resize",n.poll)}},e.fn[n.eventName]=function(e,t){return arguments.length>0?this.on(n.eventName,null,e,t):this.trigger(n.eventName)}}(jQuery,this),!function(e){"use strict";var t=e('#mainnav-menu > li > a, #mainnav-menu-wrap .mainnav-widget a[data-toggle="menu-widget"]'),n=nifty.container.children(".boxed"),i=e("#mainnav").height(),a=null,o=!1,s=!1,r=null,f=function(){var t,n=e('#mainnav-menu > li > a, #mainnav-menu-wrap .mainnav-widget a[data-toggle="menu-widget"]');n.each(function(){var i=e(this),a=i.children(".menu-title"),o=i.siblings(".collapse"),s=e(i.attr("data-target")),r=s.length?s.parent():null,l=null,f=null,c=null,d=null,m=(i.outerHeight()-i.height()/4,function(){return s.length&&i.on("click",function(e){e.preventDefault()}),o.length?(i.on("click",function(e){e.preventDefault()}).parent("li").removeClass("active"),!0):!1}()),p=null,y=function(e){clearInterval(p),p=setInterval(function(){e.nanoScroller({preventPageScrolling:!0,alwaysVisible:!0}),clearInterval(p)},700)};e(document).click(function(t){e(t.target).closest("#mainnav-container").length||i.removeClass("hover").popover("hide")}),e("#mainnav-menu-wrap > .nano").on("update",function(){i.removeClass("hover").popover("hide")}),i.popover({animation:!1,trigger:"manual",container:"#mainnav",viewport:i,html:!0,title:function(){return m?a.html():null},content:function(){var t;return m?(t=e('<div class="sub-menu"></div>'),o.addClass("pop-in").wrap('<div class="nano-content"></div>').parent().appendTo(t)):s.length?(t=e('<div class="sidebar-widget-popover"></div>'),s.wrap('<div class="nano-content"></div>').parent().appendTo(t)):t='<span class="single-content">'+a.html()+"</span>",t},template:'<div class="popover menu-popover"><h4 class="popover-title"></h4><div class="popover-content"></div></div>'}).on("show.bs.popover",function(){if(!l){if(l=i.data("bs.popover").tip(),f=l.find(".popover-title"),c=l.children(".popover-content"),!m&&0==s.length)return;d=c.children(".sub-menu")}!m&&0==s.length}).on("shown.bs.popover",function(){if(!m&&0==s.length){var t=0-.5*i.outerHeight();return void c.css({"margin-top":t+"px",width:"auto"})}var n=parseInt(l.css("top")),a=i.outerHeight(),o=function(){return nifty.container.hasClass("mainnav-fixed")?e(window).outerHeight()-n-a:e(document).height()-n-a}(),r=c.find(".nano-content").children().css("height","auto").outerHeight();c.find(".nano-content").children().css("height",""),n>o?(f.length&&!f.is(":visible")&&(a=Math.round(0-.5*a)),n-=5,c.css({top:"",bottom:a+"px",height:n}).children().addClass("nano").css({width:"100%"}).nanoScroller({preventPageScrolling:!0}),y(c.find(".nano"))):(!nifty.container.hasClass("navbar-fixed")&&nifty.mainNav.hasClass("affix-top")&&(o-=50),r>o?((nifty.container.hasClass("navbar-fixed")||nifty.mainNav.hasClass("affix-top"))&&(o-=a+5),o-=5,c.css({top:a+"px",bottom:"",height:o}).children().addClass("nano").css({width:"100%"}).nanoScroller({preventPageScrolling:!0}),y(c.find(".nano"))):(f.length&&!f.is(":visible")&&(a=Math.round(0-.5*a)),c.css({top:a+"px",bottom:"",height:"auto"}))),f.length&&f.css("height",i.outerHeight()),c.on("click",function(){c.find(".nano-pane").hide(),y(c.find(".nano"))})}).on("hidden.bs.popover",function(){i.removeClass("hover"),m?o.removeAttr("style").appendTo(i.parent()):s.length&&s.appendTo(r),clearInterval(t)}).on("click",function(){nifty.container.hasClass("mainnav-sm")&&(n.popover("hide"),i.addClass("hover").popover("show"))}).hover(function(){n.popover("hide"),i.addClass("hover").popover("show")},function(){clearInterval(t),t=setInterval(function(){l&&(l.one("mouseleave",function(){i.removeClass("hover").popover("hide")}),l.is(":hover")||i.removeClass("hover").popover("hide")),clearInterval(t)},500)})}),s=!0},c=function(){var n=e("#mainnav-menu").find(".collapse");n.length&&n.each(function(){var t=e(this);t.hasClass("in")?t.parent("li").addClass("active"):t.parent("li").removeClass("active")}),null!=a&&a.length&&a.nanoScroller({stop:!0}),t.popover("destroy").unbind("mouseenter mouseleave"),s=!1},d=function(){var n,t=nifty.container.width();n=740>=t?"xs":t>740&&992>t?"sm":t>=992&&1200>=t?"md":"lg",r!=n&&(r=n,nifty.screenSize=n,"sm"==nifty.screenSize&&nifty.container.hasClass("mainnav-lg")?e.niftyNav("collapse"):"xs"==nifty.screenSize&&nifty.container.hasClass("mainnav-lg")&&nifty.container.removeClass("mainnav-sm mainnav-out mainnav-lg").addClass("mainnav-sm"))},h=function(){nifty.mainNav.css(nifty.container.hasClass("boxed-layout")&&nifty.container.hasClass("mainnav-fixed")&&n.length?{left:n.offset().left+"px"}:{left:""})},u=function(){return nifty.mainNav.niftyAffix("update"),c(),d(),h(),("collapse"==o||nifty.container.hasClass("mainnav-sm"))&&(nifty.container.removeClass("mainnav-in mainnav-out mainnav-lg"),f()),i=e("#mainnav").height(),o=!1,null},v={revealToggle:function(){nifty.container.hasClass("reveal")||nifty.container.addClass("reveal"),nifty.container.toggleClass("mainnav-in mainnav-out").removeClass("mainnav-lg mainnav-sm"),s&&c()},revealIn:function(){nifty.container.hasClass("reveal")||nifty.container.addClass("reveal"),nifty.container.addClass("mainnav-in").removeClass("mainnav-out mainnav-lg mainnav-sm"),s&&c()},revealOut:function(){nifty.container.hasClass("reveal")||nifty.container.addClass("reveal"),nifty.container.removeClass("mainnav-in mainnav-lg mainnav-sm").addClass("mainnav-out"),s&&c()},slideToggle:function(){nifty.container.hasClass("slide")||nifty.container.addClass("slide"),nifty.container.toggleClass("mainnav-in mainnav-out").removeClass("mainnav-lg mainnav-sm"),s&&c()},slideIn:function(){nifty.container.hasClass("slide")||nifty.container.addClass("slide"),nifty.container.addClass("mainnav-in").removeClass("mainnav-out mainnav-lg mainnav-sm"),s&&c()},slideOut:function(){nifty.container.hasClass("slide")||nifty.container.addClass("slide"),nifty.container.removeClass("mainnav-in mainnav-lg mainnav-sm").addClass("mainnav-out"),s&&c()},pushToggle:function(){nifty.container.toggleClass("mainnav-in mainnav-out").removeClass("mainnav-lg mainnav-sm"),nifty.container.hasClass("mainnav-in mainnav-out")&&nifty.container.removeClass("mainnav-in"),s&&c()},pushIn:function(){nifty.container.addClass("mainnav-in").removeClass("mainnav-out mainnav-lg mainnav-sm"),s&&c()},pushOut:function(){nifty.container.removeClass("mainnav-in mainnav-lg mainnav-sm").addClass("mainnav-out"),s&&c()},colExpToggle:function(){return nifty.container.hasClass("mainnav-lg mainnav-sm")&&nifty.container.removeClass("mainnav-lg"),nifty.container.toggleClass("mainnav-lg mainnav-sm").removeClass("mainnav-in mainnav-out"),nifty.window.trigger("resize")},collapse:function(){return nifty.container.addClass("mainnav-sm").removeClass("mainnav-lg mainnav-in mainnav-out"),o="collapse",nifty.window.trigger("resize")},expand:function(){return nifty.container.removeClass("mainnav-sm mainnav-in mainnav-out").addClass("mainnav-lg"),nifty.window.trigger("resize")},togglePosition:function(){nifty.container.toggleClass("mainnav-fixed"),nifty.mainNav.niftyAffix("update")},fixedPosition:function(){nifty.container.addClass("mainnav-fixed"),nifty.mainNav.niftyAffix("update")},staticPosition:function(){nifty.container.removeClass("mainnav-fixed"),nifty.mainNav.niftyAffix("update")},update:u,forceUpdate:d,getScreenSize:function(){return r}};e.niftyNav=function(e,t){if(v[e]){("colExpToggle"==e||"expand"==e||"collapse"==e)&&("xs"==nifty.screenSize&&"collapse"==e?e="pushOut":"xs"!=nifty.screenSize&&"sm"!=nifty.screenSize||"colExpToggle"!=e&&"expand"!=e||!nifty.container.hasClass("mainnav-sm")||(e="pushIn"));var n=v[e].apply(this,Array.prototype.slice.call(arguments,1));if(u(),t)return t();if(n)return n}return null},e.fn.isOnScreen=function(){var e={top:nifty.window.scrollTop(),left:nifty.window.scrollLeft()};e.right=e.left+nifty.window.width(),e.bottom=e.top+nifty.window.height();var t=this.offset();return t.right=t.left+this.outerWidth(),t.bottom=t.top+this.outerHeight(),!(e.right<t.left||e.left>t.right||e.bottom<t.bottom||e.top>t.top)},nifty.window.on("resizeEnd",u).trigger("resize"),nifty.document.on("nifty.ready",function(){var t=e(".mainnav-toggle");t.length&&t.on("click",function(n){n.preventDefault(),e.niftyNav(t.hasClass("push")?"pushToggle":t.hasClass("slide")?"slideToggle":t.hasClass("reveal")?"revealToggle":"colExpToggle")});var n=e("#mainnav-menu");n.length&&(e("#mainnav-menu").metisMenu({toggle:!0}),a=nifty.mainNav.find(".nano"),a.length&&a.nanoScroller({preventPageScrolling:!0}))})}(jQuery),!function(e){"use strict";var t={toggleHideShow:function(){nifty.container.toggleClass("aside-in"),nifty.window.trigger("resize"),nifty.container.hasClass("aside-in")&&n()},show:function(){nifty.container.addClass("aside-in"),nifty.window.trigger("resize"),n()},hide:function(){nifty.container.removeClass("aside-in"),nifty.window.trigger("resize")
},toggleAlign:function(){nifty.container.toggleClass("aside-left"),s()},alignLeft:function(){nifty.container.addClass("aside-left"),s()},alignRight:function(){nifty.container.removeClass("aside-left"),s()},togglePosition:function(){nifty.container.toggleClass("aside-fixed"),s()},fixedPosition:function(){nifty.container.addClass("aside-fixed"),s()},staticPosition:function(){nifty.container.removeClass("aside-fixed"),s()},toggleTheme:function(){nifty.container.toggleClass("aside-bright")},brightTheme:function(){nifty.container.addClass("aside-bright")},darkTheme:function(){nifty.container.removeClass("aside-bright")},update:function(){s()}},n=function(){nifty.container.hasClass("mainnav-in")&&"xs"!=nifty.screenSize&&("sm"==nifty.screenSize?e.niftyNav("collapse"):nifty.container.removeClass("mainnav-in mainnav-lg mainnav-sm").addClass("mainnav-out"))},i=nifty.container.children(".boxed"),s=function(){nifty.aside.niftyAffix("update");var e={};e=nifty.container.hasClass("boxed-layout")&&nifty.container.hasClass("aside-fixed")&&i.length?nifty.container.hasClass("aside-left")?{"-ms-transform":"translateX("+i.offset().left+"px)","-webkit-transform":"translateX("+i.offset().left+"px)",transform:"translateX("+i.offset().left+"px)"}:{"-ms-transform":"translateX("+(0-i.offset().left)+"px)","-webkit-transform":"translateX("+(0-i.offset().left)+"px)",transform:"translateX("+(0-i.offset().left)+"px)"}:{"-ms-transform":"","-webkit-transform":"",transform:"",right:""},nifty.aside.css(e)};nifty.window.on("resizeEnd",s),e.niftyAside=function(e,n){return t[e]&&(t[e].apply(this,Array.prototype.slice.call(arguments,1)),n)?n():null},nifty.document.on("nifty.ready",function(){if(nifty.aside.length){nifty.aside.find(".nano").nanoScroller({preventPageScrolling:!0,alwaysVisible:!1});var t=e(".aside-toggle");t.length&&t.on("click",function(){e.niftyAside("toggleHideShow")})}})}(jQuery),!function(e){"use strict";var t={dynamicMode:!0,selectedOn:null,onChange:null},n=function(n,i){var a=e.extend({},t,i),o=n.find(".lang-selected"),s=o.parent(".lang-selector").siblings(".dropdown-menu"),r=s.find("a"),l=r.filter(".active").find(".lang-id").text(),f=r.filter(".active").find(".lang-name").text(),c=function(e){r.removeClass("active"),e.addClass("active"),o.html(e.html()),l=e.find(".lang-id").text(),f=e.find(".lang-name").text(),n.trigger("onChange",[{id:l,name:f}]),"function"==typeof a.onChange&&a.onChange.call(this,{id:l,name:f})};r.on("click",function(t){a.dynamicMode&&(t.preventDefault(),t.stopPropagation()),n.dropdown("toggle"),c(e(this))}),a.selectedOn&&c(e(a.selectedOn))},i={getSelectedID:function(){return e(this).find(".lang-id").text()},getSelectedName:function(){return e(this).find(".lang-name").text()},getSelected:function(){var t=e(this);return{id:t.find(".lang-id").text(),name:t.find(".lang-name").text()}},setDisable:function(){return e(this).addClass("disabled"),null},setEnable:function(){return e(this).removeClass("disabled"),null}};e.fn.niftyLanguage=function(t){var a=!1;return this.each(function(){i[t]?a=i[t].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof t&&t||n(e(this),t)}),a}}(jQuery),!function(e){"use strict";e.fn.niftyAffix=function(t){return this.each(function(){var i,n=e(this);"object"!=typeof t&&t?"update"==t&&(i=n.data("nifty.af.class")):(i=t.className,n.data("nifty.af.class",t.className)),nifty.container.hasClass(i)&&!nifty.container.hasClass("navbar-fixed")?n.affix({offset:{top:e("#navbar").outerHeight()}}):(!nifty.container.hasClass(i)||nifty.container.hasClass("navbar-fixed"))&&(nifty.window.off(n.attr("id")+".affix"),n.removeClass("affix affix-top affix-bottom").removeData("bs.affix"))})},nifty.document.on("nifty.ready",function(){nifty.mainNav.length&&nifty.mainNav.niftyAffix({className:"mainnav-fixed"}),nifty.aside.length&&nifty.aside.niftyAffix({className:"aside-fixed"})})}(jQuery);