$('#modalForm').submit(function(e){
    e.preventDefault();
    var $form = $(this);

    // get url from action attribute
    var $url = $(this).attr('action');

    $.ajax({
        method: 'POST',
        url: $url,
        data: $form.serialize()
    })
    .done(function(res){
    // make new option based on ajax response
    var option = '<option class="category" value="'+ res.id +'">'+ res.category +'</option>';
        // 

        // append option to category select
        $('#category').append(option);
    });
    $('#demo-default-modal').modal('hide');
})